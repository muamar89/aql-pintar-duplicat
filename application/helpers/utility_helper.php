<?php

define('UPLOAD_DATA_DIR', 'data/');

if (!defined('BASEPATH')) exit('No direct script access allowed');

if (!function_exists("now")) {
    function now()
    {
        return date('Y-m-d H:i:s');
    }
}


if (!function_exists("nowTime")) {
    function nowTime()
    {
        return date('H:i:s');
    }
}


if (!function_exists("printJson")) {
    function printJson($controller, $res)
    {
        return $controller->output->set_content_type('application/json')->set_output(json_encode($res));
    }
}


if (!function_exists("uppercaseTrim")) {
    function uppercaseTrim($source)
    {
        return strtoupper(trim($source));
    }
}


function safe_b64encode($string)
{
    $data = base64_encode($string);
    $data = str_replace(array('+', '/', '='), array('-', '_', ''), $data);
    return $data;
}

function safe_b64decode($string)
{
    $data = str_replace(array('-', '_'), array('+', '/'), $string);
    $mod4 = strlen($data) % 4;
    if ($mod4) {
        $data .= substr('====', $mod4);
    }
    return base64_decode($data);
}

function encryptEncode($value)
{
    if (!$value) {
        return false;
    }
    $text = $value;
    $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
    $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
    $crypttext = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, SKEY, $text, MCRYPT_MODE_ECB, $iv);
    return trim(safe_b64encode($crypttext));
}

function encryptDecode($value)
{
    if (!$value) {
        return false;
    }
    $crypttext = safe_b64decode($value);
    $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
    $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
    $decrypttext = mcrypt_decrypt(MCRYPT_RIJNDAEL_256, SKEY, $crypttext, MCRYPT_MODE_ECB, $iv);
    return trim($decrypttext);
}

if (!function_exists("getDayOfWeek")) {
    function getDayOfWeek($booking_date)
    {
        return date('w', strtotime($booking_date));
    }
}

if (!function_exists("readableDate")) {
    function readableDate($date)
    {
        return date("d M Y", strtotime($date));
    }
}

if (!function_exists("readableDateTime")) {
    function readableDateTime($datetime)
    {
        return date("D, d M Y H:m", strtotime($datetime));
    }
}

if (!function_exists("thousandSeparator")) {
    function thousandSeparator($price)
    {
        return number_format($price, 0, ",", ".");
    }
}

if (!function_exists('generateRandomCode')) {
    function generateRandomCode($length = 50)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ' . md5(uniqid(rand(), true));
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}

if (!function_exists('sendPushNotification')) {
    function sendPushNotification($userTokenId, $title, $body)
    {
        $serverKey = 'AAAA7SWp-fw:APA91bFNAVo4FlWqGaR1mx4o_W6FNGm_81tpMO-mLbjRnc3tfHeSB2SrslzPcgO0h3aSWC_gqZld83Vswpy7pFN6R9NYZwx3bHLqjQV5MXIizvyTtY3wj7CUgHoU1PZ8NhTJsuaTLAqY';
        $pusher = array(
            'title' => $title,
            'body' => $body,
            'click_action' => 'OPEN_ACTIVITY_NOTIFICATION'
        );
        $fields = array(
            'to' => $userTokenId,
            'notification' => $pusher,
            'data' => $pusher,
            'icon' => 'ic_notification'
        );
        $headers = array
        (
            'Authorization: key=' . $serverKey,
            'Content-Type: application/json'
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        curl_exec($ch);
        curl_close($ch);
    }
}

if (!function_exists('isValidMd5')) {
    function isValidMd5($md5 = '')
    {
        return preg_match('/^[a-f0-9]{32}$/', $md5);
    }
}

if (!function_exists('getContent')) {
    function getContentBnis($url, $post = '')
    {
        $usecookie = __DIR__ . "/cookie.txt";
        $header[] = 'Content-Type: application/json';
        $header[] = "Accept-Encoding: gzip, deflate";
        $header[] = "Cache-Control: max-age=0";
        $header[] = "Connection: keep-alive";
        $header[] = "Accept-Language: en-US,en;q=0.8,id;q=0.6";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        // curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_VERBOSE, false);
        // curl_setopt($ch, CURLOPT_NOBODY, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_ENCODING, true);
        curl_setopt($ch, CURLOPT_AUTOREFERER, true);
        curl_setopt($ch, CURLOPT_MAXREDIRS, 5);

        curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.120 Safari/537.36");

        if ($post) {
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        }

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $rs = curl_exec($ch);

        if (empty($rs)) {
//            var_dump($rs, curl_error($ch));
            curl_close($ch);
            return false;
        }
        curl_close($ch);
        return $rs;
    }
}
if (!function_exists('terbilang')) {
    function terbilang($x)
    {
        $angka = ["", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas"];

        if ($x < 12)
            return " " . $angka[$x];
        elseif ($x < 20)
            return terbilang($x - 10) . " belas";
        elseif ($x < 100)
            return terbilang($x / 10) . " puluh" . terbilang($x % 10);
        elseif ($x < 200)
            return " seratus" . terbilang($x - 100);
        elseif ($x < 1000)
            return terbilang($x / 100) . " ratus" . terbilang($x % 100);
        elseif ($x < 2000)
            return " seribu" . terbilang($x - 1000);
        elseif ($x < 1000000)
            return terbilang($x / 1000) . " ribu" . terbilang($x % 1000);
        elseif ($x < 1000000000)
            return terbilang($x / 1000000) . " juta" . terbilang($x % 1000000);
    }
}

if (!function_exists('sendMessageText')) {
    function sendMessageText($to, $message)
    {
        $url = WA_URL . '/api/send-message.php';

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, array(
            'token' => WA_TOKEN,
            'phone' => $to,
            'message' => $message,
        ));
        $response = curl_exec($curl);
        curl_close($curl);
    }
}

if (!function_exists('sendMessageDocument')) {
    function sendMessageDocument($to, $link, $fileName, $caption = '')
    {
        $url = WA_URL . '/send-document.php';

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, array(
            'token' => WA_TOKEN,
            'phone' => $to,
            'document' => $link,
            'filename' => $fileName,
            'caption' => $caption,
        ));
        $response = curl_exec($curl);
        curl_close($curl);
    }
}

if (!function_exists('clean')) {
    function clean($string)
    {
        $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
        return preg_replace('/[^A-Za-z0-9\-.]/', '_', $string); // Removes special chars.
    }
}

if (!function_exists("thousandSeparator")) {
    function thousandSeparator($price)
    {
        return number_format($price, 0, ",", ".");
    }
}

if (!function_exists("rupiah")) {
    function rupiah($price)
    {
        return "Rp." . number_format($price, 0, ",", ".");
    }
}

if (!function_exists("generateVaNumber")) {
    function generateVaNumber($suffix)
    {
        return BNIS_PREFIX . BNIS_CLIENT_ID . $suffix;
    }
}

if (!function_exists("generateKwitansiProgram")) {
    function generateKwitansiProgram($program_user_id)
    {
        $CI = get_instance();
        $CI->load->model('ProgramUserModel');
        $CI->load->model('ProgramModel');
        $CI->load->model('UserModel');
        include_once APPPATH . '/third_party/mpdf/mpdf.php';

        if ($program_user_id) {
            $programUserModel = new ProgramUserModel();
            $programUser = $programUserModel->getById($program_user_id);
            $data['programUser'] = $programUser;

            $programModel = new ProgramModel();
            $program = $programModel->getById($programUser->program_id);
            $data['program'] = $program;

            $userModel = new UserModel();
            $data['user'] = $userModel->getById($programUser->user_id);

            $html = $CI->load->view('KwitansiProgramView', $data, true);

            $mPDF = new mPDF('c', 'A5-L', 0, '', 5, 5, 5, 0);
            $mPDF->WriteHTML($html);
            $filename = str_replace(array('/', ' '), '_', $programUser->invoice_no) . '.pdf';
            $filePath = './data/' . $filename;

            if (file_exists($filePath)) {
                unlink($filePath);
            }

            $mPDF->Output($filePath, 'F');

            return base_url() . 'data/' . $filename;
        }
    }
}