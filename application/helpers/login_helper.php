<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
if (!function_exists("isLoggedIn")) {

    function isLoggedIn()
    {
        $CI = &get_instance();
        $email = $CI->session->userdata('email');

        if (!isset($email) || $email != true) {
            return false;
        } else {
            return true;
        }
    }
}

if (!function_exists("getUserRole")) {

    function getUserRole()
    {
        $CI = &get_instance();
        $role = $CI->session->userdata('role');

        return $role;
    }
}

if (!function_exists("getSessionEmail")) {

    function getSessionEmail()
    {
        $CI = &get_instance();
        $email = $CI->session->userdata('email');

        return $email;
    }
}

if (!function_exists("getSessionRole")) {

    function getSessionRole()
    {
        $CI = &get_instance();
        $email = $CI->session->userdata('role');

        return $email;
    }
}

if (!function_exists("getSessionNama")) {

    function getSessionNama()
    {
        $CI = &get_instance();
        $email = $CI->session->userdata('nama');

        return $email;
    }
}

if (!function_exists("getSessionUserId")) {

    function getSessionUserId()
    {
        $CI = &get_instance();
        $user_id = $CI->session->userdata('user_id');

        return $user_id;
    }
}

if (!function_exists("getSessionMemberId")) {

    function getSessionMemberId()
    {
        $CI = &get_instance();
        $user_id = $CI->session->userdata('member_id');

        return $user_id;
    }
}

if (!function_exists("getSessionId")) {

    function getSessionId()
    {
        $CI = &get_instance();
        $user_id = $CI->session->userdata('session_id');

        return $user_id;
    }
}

if (!function_exists("getSessionAvatar")) {

    function getSessionAvatar()
    {
        $CI = &get_instance();
        $avatar = $CI->session->userdata('avatar');

        return $avatar;
    }
}

if (!function_exists("getSessionPhoneNumber")) {

    function getSessionPhoneNumber()
    {
        $CI = &get_instance();
        $no_hp = $CI->session->userdata('no_hp');

        return $no_hp;
    }
}

if (!function_exists("getSessionActiveSince")) {

    function getSessionActiveSince()
    {
        $CI = &get_instance();
        $active_since = $CI->session->userdata('active_since');

        return $active_since;
    }
}

if (!function_exists("getSessionLocationId")) {

    function getSessionLocationId()
    {
        $CI = &get_instance();
        $user_id = $CI->session->userdata('location_id');

        return $user_id;
    }
}

if (!function_exists("getCapturePhotoRequired")) {

    function getCapturePhotoRequired()
    {
        $CI = &get_instance();
        $capture_photo_required = $CI->session->userdata('capture_photo_required');

        return $capture_photo_required;
    }
}
