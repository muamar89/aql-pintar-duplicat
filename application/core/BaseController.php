<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class BaseController extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('UserActivityModel');

        if (!isLoggedIn()) {
            $redirect = urlencode(base_url(uri_string()));
            redirect(base_url() . "login?redirect=$redirect");
        } else {
            /*$userActivityModel = new UserActivityModel();
            $userActivityModel->session_id = $this->session->userdata('app_session_id');
            $userActivityModel->email = $this->session->userdata('email');
            $userActivityModel->ip_address = $_SERVER['REMOTE_ADDR'];
            $userActivityModel->user_agent = $_SERVER['HTTP_USER_AGENT'];
            $userActivityModel->page = $this->router->fetch_class() . "/" . $this->router->fetch_method();
            $userActivityModel->dbInsert($userActivityModel->fetch());*/
        }
    }
}
