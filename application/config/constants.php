<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/* SITE CONSTANTS */

define('JQGRID_PAGE_START', 1);
define('JQGRID_PAGE_LIMIT', 10);
define('JQGRID_SORT_ORDER', 'desc');
define('JQGRID_ROW_LIST', '10, 20, 30');

/* SITE CONSTANTS */

define('BNIS_PREFIX', '988');
define('BNIS_CLIENT_ID', '04456');
define('BNIS_SECRET_KEY', 'da7bf89a4622a0fcce92da99890dfc92');
define('BNIS_URL', 'https://apibeta.bni-ecollection.com/');
define('WA_TOKEN', 'cgyN5FpczptvDQj35USARye1cNQoUdLINFXf8BZd6rXQ7K4lWy');
define('WA_URL', 'http://apps.pandawa.my.id');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ', 'rb');
define('FOPEN_READ_WRITE', 'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE', 'ab');
define('FOPEN_READ_WRITE_CREATE', 'a+b');
define('FOPEN_WRITE_CREATE_STRICT', 'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');


/* End of file constants.php */
/* Location: ./application/config/constants.php */