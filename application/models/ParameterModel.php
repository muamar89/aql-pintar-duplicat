<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class ParameterModel extends BaseModel
{
    function ParameterModel()
    {
        parent::__construct();
        $this->tableName = 'parameter';
        $this->primaryKeyName = 'parameter_id';
    }
}