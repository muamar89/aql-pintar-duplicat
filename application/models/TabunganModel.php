<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class TabunganModel extends BaseModel
{
    function TabunganModel()
    {
        parent::__construct();
        $this->tableName = 'tabungan';
        $this->primaryKeyName = 'tabungan_id';
    }

    function getLastNoTabungan($year)
    {
        $query = $this->db->query("select SUBSTR(no_tabungan, 3, 6) no_tabungan from tabungan where created_date like '%$year%' order by tabungan_id desc");
        $result = $query->result();
        return (count($result) > 0 ? $result[0]->no_tabungan : NULL);
    }

    function isTabunganExist($user_id)
    {
        $this->db->where('user_id', $user_id);
        $this->db->limit(1);
        $query = $this->db->get($this->tableName);
        return ($query->num_rows() > 0) ? true : false;
    }

    function getByUser($user_id)
    {
        $this->db->where('user_id', $user_id);
        $query = $this->db->get($this->tableName);
        $result = $query->result();
        return (count($result) > 0 ? $result[0] : NULL);
    }

    function getSaldo($userId = NULL)
    {
        if ($userId != NULL) {
            $query = $this->db->query("select ifnull(sum(ifnull(kredit, 0)) - sum(ifnull(debit, 0)), 0) as saldo from saldo_tabungan where user_id = $userId");
        } else {
            $query = $this->db->query("select ifnull(sum(ifnull(kredit, 0)) - sum(ifnull(debit, 0)), 0) as saldo from saldo_tabungan");
        }

        $result = $query->result();
        return $result[0]->saldo;
    }

}