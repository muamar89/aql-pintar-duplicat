<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class ParameterDetailModel extends BaseModel
{
    function ParameterDetailModel()
    {
        parent::__construct();
        $this->tableName = 'parameter_detail';
        $this->primaryKeyName = 'parameter_detail_id';
    }

    function getByParameter($parameterId)
    {
        $this->db->where('parameter_id', $parameterId);
        $query = $this->db->get($this->tableName);
        return $query->result();
    }
}