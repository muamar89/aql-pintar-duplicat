<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class KelasModel extends BaseModel
{
    function KelasModel()
    {
        parent::__construct();
        $this->tableName = 'kelas';
        $this->primaryKeyName = 'kelas_id';
    }
}
