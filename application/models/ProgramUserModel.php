<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class ProgramUserModel extends BaseModel
{
     function ProgramUserModel()
    {
        parent::__construct();
        $this->tableName = 'program_user';
        $this->primaryKeyName = 'program_user_id';
    }

    function getByUser($userID)
    {
        $this->db->where('user_id', $userID);
        $this->db->order_by('program_user_id', 'desc');
        $query = $this->db->get($this->tableName);

        $result = $query->result();
        return (count($result) > 0 ? $result : NULL);
    }

    function getByInvoiceNo($invoiceNo)
    {
        $this->db->where('invoice_no', $invoiceNo);
        $query = $this->db->get($this->tableName);
        $result = $query->result();
        return (count($result) > 0 ? $result[0] : NULL);
    }
}