<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class ProgramJamaahModel extends BaseModel
{
     function ProgramJamaahModel()
    {
        parent::__construct();
        $this->tableName = 'program_jamaah';
        $this->primaryKeyName = 'program_jamaah_id';
    }

    function getByJamaah($program_user_id, $jamaah_id)
    {
        $this->db->where('program_user_id', $program_user_id);
        $this->db->where('jamaah_id', $jamaah_id);
        $query = $this->db->get($this->tableName);
        $result = $query->result();
        return (count($result) > 0 ? $result[0] : NULL);
    }


    function getTotalRegisteredJamaahByProgramUser($programUserId)
    {
        $query = $this->db->query("select ifnull(count(*), 0) as total_registered
                    from program_user left join program_jamaah on program_jamaah.program_user_id = program_user.program_user_id
                    where program_user.program_user_id = $programUserId");

        $result = $query->result();
        return $result[0]->total_registered;
    }

}