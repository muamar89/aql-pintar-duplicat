<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class WebhookModel extends BaseModel
{
    function WebhookModel()
    {
        parent::__construct();
        $this->tableName = 'webhook';
        $this->primaryKeyName = 'webhook_id';
    }
}