<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class UserActivityModel extends BaseModel
{

    function UserActivityModel()
    {
        parent::__construct();
        $this->tableName = 'user_activity';
        $this->primaryKeyName = 'session_id';
    }

    function logout($session_id)
    {
        $data = array(
            'logout_date' => now());

        $this->db->where('session_id', $session_id);
        $this->db->update($this->tableName, $data);
    }
}