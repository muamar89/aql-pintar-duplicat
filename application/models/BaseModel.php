<?php

/**
 * Created by JetBrains PhpStorm.
 * User: Doni Wahyu Shahri Romadhona
 * Date: 11/13/14
 * Time: 11:31 AM
 * To change this template use File | Settings | File Templates.
 */
class BaseModel extends CI_Model
{
    protected $dbms;
    private $rowFrom = 0;
    private $rowTo = 0;
    private $findQuery;

    // For Table
    protected $tableName;
    protected $columnId;
    protected $sequenceName;
    protected $primaryKeyName;

    private $lastcreatedId;
    static $userId;
    private $getLoginId;

    public function BaseModel()
    {
        parent::__construct();
    }

    function getLastcreatedId()
    {
        $this->db->select_max($this->primaryKeyName);
        $result = $this->db->get($this->tableName)->row_array();
        return $result[$this->primaryKeyName];
    }

    function getById($key)
    {
        $this->db->where($this->primaryKeyName, $key);
        $query = $this->db->get($this->tableName);
        $result = $query->result();
        return (count($result) > 0 ? $result[0] : NULL);
    }

    function delete($id)
    {
        $this->db->where($this->primaryKeyName, $id);
        $this->db->delete($this->tableName);
    }

    function getAll($where, $sort, $order, $limit, $page)
    {

        if (count($where) > 0) {
            foreach ($where as $filter) {
                foreach ($filter as $key => $value) {
                    $this->db->where($key, $value);
                }
            }
        }

        $this->db->order_by($sort, $order);

        $start = $limit * $page - $limit;
        if ($start < 0) $start = 0;

        return $this->db->get($this->tableName, $limit, $start)->result();
    }

    function countAll($where)
    {
        if (count($where) > 1) {
            foreach ($where as $filter) {
                foreach ($filter as $key => $value) {
                    $this->db->where($key, $value);
                }
            }
        }
        $query = $this->db->get($this->tableName);

        return $query->num_rows();
    }


    function dbGetAll($sortField = NULL, $oder = NULL, $is_active = NULL)
    {
        $this->db->from($this->tableName);

        if ($sortField == NULL) {
            $this->db->order_by($this->primaryKeyName, "asc");
        } else {
            $this->db->order_by($sortField, ($oder == NULL) ? "asc" : $oder);
        }

        if ($is_active != NULL) {
            $this->db->where('is_active', $is_active);
        }

        $query = $this->db->get();
        return $query->result();
    }

    function dbGetRow($query, $arr = array())
    {
        //$sql = $this->dbms->execList($query);
        $rs = $this->db->query($query, $arr);
        //  print_r($rs->result_array());
        return $rs->row();
    }

    function dbGetRows($query, $arr = array())
    {
        //$sql = $this->dbms->execList($query);
        $rs = $this->db->query($query, $arr);
//        return ($rs->result_array());
        return ($rs->result());
    }

    function fetch()
    {
        return call_user_func('get_object_vars', $this);
    }

    function exec($query, $getValue)
    {
        return $this->db->query($query, $getValue);
    }

    private function add($arrColumns = array())
    {
        $sql = " INSERT INTO " . $this->tableName . " (";
        $sql .= implode(",", $arrColumns) . ")";

        $sql .= " VALUES(";
        //$sql .= implode(",", $arrayValue).")";
        $i = 1;
        foreach ($arrColumns as $columns) {
            $sql .= "?";
            if ($i < count($arrColumns)) {
                $sql .= ",";
            }
            $i++;
        }
        $sql .= ")";
        /*if ($this->primaryKeyName == "")
            throw new Exception('<div style="background-color:yellow">No Primary Key Name defined in your Model'
                . '<br>Put in your model <code>$this->primaryKeyName = "YOUR_PRIMARY_KEY_COLUMN"</code>;'
                . '</div>');
        if ($this->sequenceName == "")
            throw new Exception('<div style="background-color:yellow">No Sequence Name defined in your Model'
                . '<br>Put in your model <code>$this->sequenceName = "YOUR_SEQUENCE_NAME"</code>;'
                . '</div>');*/
        return $sql;
    }

    function dbInsert($arrColumnsVal = array())
    {
        $this->db->trans_begin();

        $arrToColumns = array();
        $getValue = array();
        $getKey = array();

        foreach ($arrColumnsVal as $key => $val) {
            $getValue[] = $val;
            $arrToColumns[] = '?';
            $getKey[] = $key;
        }


        $query = $this->add($getKey);
        $this->exec($query, $getValue);
        //return status

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
        return ($this->db->affected_rows() != 1) ? false : true;
    }


    function dbUpdate($data = array(), $key)
    {
        $this->db->trans_begin();

        $this->db->update($this->tableName, $data, array($this->primaryKeyName => $key));

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
//        return ($this->db->affected_rows() != 1) ? false : true;
    }

    function dbUpdateArry($data = array(), $where = array())
    {
        $this->db->trans_begin();

        $this->db->update($this->tableName, $data, $where);

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
//        return ($this->db->affected_rows() != 1) ? false : true;
    }

    function dbDeleteById($id)
    {
        $this->db->where($this->primaryKeyName, $id);
        $this->db->delete($this->tableName);

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    function dbDelete($arrWhere = array())
    {
        $this->db->trans_begin();

        $this->db->delete($this->tableName, $arrWhere);

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
//            return ($this->db->affected_rows() != 1) ? false : true;
    }
}

?>
