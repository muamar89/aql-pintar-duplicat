<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class KotaModel extends BaseModel
{
    function KotaModel()
    {
        parent::__construct();
        $this->tableName = 'kota';
        $this->primaryKeyName = 'id';
    }

    function getByProvinsi($provinsiId)
    {
        $this->db->where('provinsi_id', $provinsiId);
        $query = $this->db->get($this->tableName);
        return $query->result();
    }
}