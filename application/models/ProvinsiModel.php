<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class ProvinsiModel extends BaseModel
{
    function ProvinsiModel()
    {
        parent::__construct();
        $this->tableName = 'provinsi';
        $this->primaryKeyName = 'id';
    }
}