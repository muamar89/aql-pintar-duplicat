<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class SetorTabunganModel extends BaseModel
{
    function SetorTabunganModel()
    {
        parent::__construct();
        $this->tableName = 'setor_tabungan';
        $this->primaryKeyName = 'setor_tabungan_id';
    }

    function getByUser($user_id)
    {
        $query = "select setor_tabungan.* from setor_tabungan
                  inner join tabungan on tabungan.tabungan_id = setor_tabungan.tabungan_id
                  where tabungan.user_id = $user_id";
        return $this->db->query($query)->result();
    }

    function getLastSetoran($invoiceNo)
    {
        $this->db->where('invoice_no', $invoiceNo);
        $this->db->order_by('setor_tabungan_id', 'desc');
        $query = $this->db->get($this->tableName);
        $result = $query->result();
        return (count($result) > 0 ? $result[0] : NULL);
    }

    function getFirstSetoran($invoiceNo)
    {
        $this->db->where('invoice_no', $invoiceNo);
        $this->db->order_by('setor_tabungan_id', 'asc');
        $query = $this->db->get($this->tableName);
        $result = $query->result();
        return (count($result) > 0 ? $result[0] : NULL);
    }
}