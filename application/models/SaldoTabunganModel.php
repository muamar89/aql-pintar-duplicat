<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class SaldoTabunganModel extends BaseModel
{
    function SaldoTabunganModel()
    {
        parent::__construct();
        $this->tableName = 'saldo_tabungan';
        $this->primaryKeyName = 'saldo_tabungan_id';
    }

    function isSaldoTabunganExist($user_id)
    {
        $this->db->where('user_id', $user_id);
        $this->db->limit(1);
        $query = $this->db->get($this->tableName);
        return ($query->num_rows() > 0) ? true : false;
    }

    function insertSaldo($setor_tabungan_id, $user_id, $kredit = 0, $debit = 0, $deskripsi = NULL)
    {
        $datas = array(
            'setor_tabungan_id' => $setor_tabungan_id,
            'user_id' => $user_id,
            'kredit' => $kredit,
            'debit' => $debit,
            'deskripsi' => $deskripsi,
        );
        $this->db->insert('saldo_tabungan', $datas);
    }

}