<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tabungan extends BaseController
{
    var $title = "TABUNGAN";
    var $cUri = "tabungan";
    var $menuName = "Tabungan";

    function __construct()
    {
        parent::__construct();
        $this->load->model('UserModel');
        $this->load->model('ProvinsiModel');
        $this->load->model('ParameterDetailModel');
        $this->load->model('TabunganModel');
        $this->load->model('SaldoTabunganModel');
        $this->load->model('SetorTabunganModel');
        $this->load->model('WebhookModel');

        $this->load->helper('url');
        $this->load->library('BniEnc');
    }

    public function index()
    {
        $tabunganModel = new TabunganModel();
        if (!$tabunganModel->isTabunganExist(getSessionUserId())) {
            redirect(base_url('tabungan/daftar'));
        }

        $data['title'] = 'Dashboard';

        $tabungan = new TabunganModel();
        $data['saldo'] = $tabungan->getSaldo(getSessionUserId());

        $tabungan = new TabunganModel();
        $data['tabungan'] = $tabungan->getByUser(getSessionUserId());

        $setorTabunganModel = new SetorTabunganModel();
        $data['setorTabunganList'] = $setorTabunganModel->getByUser(getSessionUserId());

        $this->load->view('HeaderView', $data);
        $this->load->view('TabunganView', $data);
        $this->load->view('FooterView', $data);
    }

    public function invoice($id)
    {
        $data['title'] = 'Invoice';

        if ($id) {
            $setorTabunganModel = new SetorTabunganModel();
            $setorTabungan = $setorTabunganModel->getById($id);

            $tabungan = new TabunganModel();
            $data['tabungan'] = $tabungan->getByUser(getSessionUserId());

            $data['setorTabungan'] = $setorTabungan;

            $this->load->view('HeaderView', $data);
            $this->load->view('InvoiceView', $data);
            $this->load->view('FooterView', $data);
        }
    }

    public function daftar()
    {
        $data['title'] = 'Daftar Tabungan';

        $this->load->view('HeaderView', $data);
        $this->load->view('DaftarTabunganView', $data);
        $this->load->view('FooterView', $data);
    }

    public function setor()
    {
        $data['title'] = 'Setor Tabungan';

        $placeHolderNominal = "Nominal";
        $minNominal = "0";
        $saldoTabunganModel = new SaldoTabunganModel();
        if (!$saldoTabunganModel->isSaldoTabunganExist(getSessionUserId())) {
            $parameterDetail = new ParameterDetailModel();
            $parameterDetailList = $parameterDetail->getById(9);

            $placeHolderNominal = "Setoran Awal Minimal Rp." . thousandSeparator($parameterDetailList->value);
            $minNominal = $parameterDetailList->value;
        }

        $data['placeHolderNonimal'] = $placeHolderNominal;
        $data['minNominal'] = $minNominal;

        $this->load->view('HeaderView', $data);
        $this->load->view('SetorTabunganView', $data);
        $this->load->view('FooterView', $data);
    }

    public function provinsi()
    {
        $provinsi = new ProvinsiModel();
        $provinsiList = $provinsi->dbGetAll();

        $data = array();
        foreach ($provinsiList as $provinsi) {
            $tmp = array();
            $tmp['id'] = $provinsi->name;
            $tmp['text'] = $provinsi->name;
            array_push($data, $tmp);
        }

        $response = array('success' => true, 'data' => $data);
        printJson($this, $response);
    }

    public function targettabungan()
    {
        $data = array();

        $parameterDetail = new ParameterDetailModel();
        $parameterDetailList = $parameterDetail->getByParameter(1);

        foreach ($parameterDetailList as $parameterDetail) {
            $tmp = array();
            $tmp['id'] = $parameterDetail->value;
            $tmp['text'] = "Rp." . thousandSeparator($parameterDetail->value);
            array_push($data, $tmp);
        }

        $response = array('success' => true, 'data' => $data);

        printJson($this, $response);
    }

    public function jangkawaktu()
    {
        $data = array();

        $parameterDetail = new ParameterDetailModel();
        $parameterDetailList = $parameterDetail->getByParameter(2);

        foreach ($parameterDetailList as $parameterDetail) {
            $tmp = array();
            $tmp['id'] = $parameterDetail->value;
            $tmp['text'] = $parameterDetail->value . " Bln";
            array_push($data, $tmp);
        }

        $response = array('success' => true, 'data' => $data);

        printJson($this, $response);
    }

    public function jumlahkeluarga()
    {
        $data = array();

        for ($i = 1; $i <= 11; $i++) {
            $tmp = array();

            if ($i == 1) {
                $text = "Saya Sendiri";
            } else {
                $text = "Saya & " . ($i - 1) . " Anggota Keluarga";
            }

            $tmp['id'] = $i;
            $tmp['text'] = $text;
            array_push($data, $tmp);
        }

        $response = array('success' => true, 'data' => $data);

        printJson($this, $response);
    }

    function generateNewNoTabungan()
    {
        $tabunganModel = new TabunganModel();
        $lastNoTabungan = $tabunganModel->getLastNoTabungan(date('Y'));

        if ($lastNoTabungan == NULL) {
            $newNumber = date('y') . "000001";
        } else {
            $lastNumber = "0000000" . (((int)substr($lastNoTabungan, 0, 6)) + 1);
            $newNumber = date('y') . substr($lastNumber, -6);
        }

        return $newNumber;
    }

    function generateVaNumber($noTabungan)
    {
        return BNIS_PREFIX . BNIS_CLIENT_ID . $noTabungan;
    }

    function doDaftar()
    {
        $nama_lengkap = trim($this->input->post('nama_lengkap'));
        $jenis_kelamin = trim($this->input->post('jenis_kelamin'));
        $tempat_lahir = trim($this->input->post('tempat_lahir'));
        $tanggal_lahir = trim($this->input->post('tanggal_lahir'));
        $bulan_lahir = trim($this->input->post('bulan_lahir'));
        $tahun_lahir = trim($this->input->post('tahun_lahir'));
        $alamat = trim($this->input->post('alamat'));
        $provinsi_id = trim($this->input->post('provinsi_id'));
        $kota_id = trim($this->input->post('kota_id'));
        $target_tabungan = trim($this->input->post('target_tabungan'));
        $jangka_waktu = trim($this->input->post('jangka_waktu'));
        $jumlah_keluarga = trim($this->input->post('jumlah_keluarga'));

        if ($nama_lengkap != '' && $jenis_kelamin != '' && $tempat_lahir != ''
            && $alamat != '' && $provinsi_id != '' && $kota_id != '' && $target_tabungan != '' && $jangka_waktu != '' && $jumlah_keluarga != ''
        ) {
            $tabungan = new TabunganModel();
            $no_tabungan = $this->generateNewNoTabungan();
            $va = $this->generateVaNumber($no_tabungan);
            $tabungan->no_tabungan = $no_tabungan;
            $tabungan->va_bnis = $va;
            $tabungan->user_id = getSessionUserId();
            $tabungan->nama_lengkap = $nama_lengkap;
            $tabungan->jenis_kelamin = $jenis_kelamin;
            $tabungan->tempat_lahir = $tempat_lahir;
            $tabungan->tanggal_lahir = $tahun_lahir . "-" . $bulan_lahir . "-" . $tanggal_lahir;
            $tabungan->alamat = $alamat;
            $tabungan->provinsi_id = $provinsi_id;
            $tabungan->kota_id = $kota_id;
            $tabungan->target_tabungan = $target_tabungan;
            $tabungan->jangka_waktu = $jangka_waktu;
            $tabungan->jumlah_keluarga = $jumlah_keluarga;
            $tabungan->setoran_bulanan = ($target_tabungan * $jumlah_keluarga) / $jangka_waktu;
            $tabungan->created_by = getSessionUserId();
            $tabungan->created_date = now();

            if ($tabungan->dbInsert($tabungan->fetch())) {
                $tabungan = new TabunganModel();
                $tabungan = $tabungan->getByUser(getSessionUserId());

                if ($tabungan != null) {
                    $user = new UserModel();
                    $user = $user->getById(getSessionUserId());

                    $parameterDetail = new ParameterDetailModel();
                    $parameterDetailList = $parameterDetail->getById(9);
                    $nominal = $parameterDetailList->value;

                    $setorTabunganId = $this->generateInvoice($user, $tabungan, $nominal);

                    if ($setorTabunganId != NULL) {
                        $parameterDetail = new ParameterDetailModel();
                        $parameterDetailList = $parameterDetail->getById(11);
                        $message = $parameterDetailList->value;

                        $user = new UserModel();
                        $user = $user->getById(getSessionUserId());

                        $message = str_replace(array('{nama}'), array($nama_lengkap), $message);
                        $message = str_replace(array('{no_hp}'), array($user->no_hp), $message);
                        $message = str_replace(array('{alamat}'), array($alamat), $message);
                        $message = str_replace(array('{email}'), array($user->email), $message);
                        $message = str_replace(array('{member_id}'), array($user->member_id), $message);
                        $message = str_replace(array('{program}'), array('Tabungan Berkah'), $message);
                        $message = str_replace(array('{va}'), array($va), $message);
                        $message = str_replace(array('{nominal}'), array('Rp.' . thousandSeparator($nominal)), $message);

                        sendMessageText($user->no_hp, $message);

                        $response = array('success' => true, 'message' => 'Registrasi Berhasil', 'redirect' => base_url('/tabungan/invoice/' . $setorTabunganId));
                    } else {
                        $response = array('success' => false, 'message' => 'Setor Tabungan Gagal, harap hubungi Admin');
                    }
                } else {
                    $response = array('success' => false, 'message' => 'Registrasi Gagal');
                }
            } else {
                $response = array('success' => false, 'message' => 'Registrasi Gagal');
            }

        } else {
            $response = array('success' => false, 'message' => 'Silahkan lengkapi semua data');
        }


        printJson($this, $response);
    }

    function generateInvoice($user, $tabungan, $nominal)
    {
        $setorTabunganId = NULL;

        $trx_id = "INV/" . $tabungan->no_tabungan . "/" . hexdec(substr(uniqid(),0,8));
        $data_asli = array(
            'client_id' => BNIS_CLIENT_ID,
            'trx_id' => $trx_id,
            'trx_amount' => $nominal,
            'billing_type' => 'n',
            'virtual_account' => $tabungan->va_bnis,
            'customer_name' => $tabungan->nama_lengkap,
            'customer_email' => $user->email,
            'customer_phone' => $user->no_hp,
            'datetime_expired' => '2048-12-31T23:00:00+07:00',
            'type' => 'createBilling'
        );

        $hashed_string = BniEnc::encrypt(
            $data_asli,
            BNIS_CLIENT_ID,
            BNIS_SECRET_KEY
        );

        $data = array(
            'client_id' => BNIS_CLIENT_ID,
            'data' => $hashed_string,
        );

        $response = getContentBnis(BNIS_URL, json_encode($data));
        $response_json = json_decode($response, true);

        $data_response = BniEnc::decrypt($response_json['data'], BNIS_CLIENT_ID, BNIS_SECRET_KEY);

        $webHook = new WebhookModel();
        $webHook->source = 'va_bnis';
        $webHook->data = json_encode($data_response, true);
        $webHook->dbInsert($webHook->fetch());

        if ($response_json['status'] == '000') {
            // $data_response['virtual_account']
            $setorTabungan = new SetorTabunganModel();
            $setorTabungan->tabungan_id = $tabungan->tabungan_id;
            $setorTabungan->invoice_no = $data_response['trx_id'];
            $setorTabungan->deskripsi = 'Setoran Awal';
            $setorTabungan->nominal = $nominal;
            $setorTabungan->user_id = getSessionUserId();
            $setorTabungan->created_by = getSessionUserId();
            $setorTabungan->created_date = now();

            if ($setorTabungan->dbInsert($setorTabungan->fetch())) {
                $setorTabunganId = $setorTabungan->getLastcreatedId();
            }
        }


//        $setorTabungan = new SetorTabunganModel();
//        $setorTabungan->user_id = $tabungan->user_id;
//        $setorTabungan->tabungan_id = $tabungan->tabungan_id;
//        $setorTabungan->setoran_ke = 1;
//        $setorTabungan->invoice_no = $trx_id;
//        $setorTabungan->nominal = $nominal;
//        $setorTabungan->created_by = getSessionUserId();
//        $setorTabungan->created_date = now();
//
//        if ($setorTabungan->dbInsert($setorTabungan->fetch())) {
//            $setorTabunganId = $setorTabungan->getLastcreatedId();
//        }

        return $setorTabunganId;
    }

    function doSetor()
    {
        $nominal = trim($this->input->post('nominal'));

        if ($nominal) {
            $tabungan = new TabunganModel();
            $tabungan = $tabungan->getByUser(getSessionUserId());

            if ($tabungan != null) {
                $user = new UserModel();
                $user = $user->getById(getSessionUserId());

                $setorTabunganId = $this->generateInvoice($user, $tabungan, $nominal);
                if ($setorTabunganId != NULL) {
                    $response = array('success' => true, 'redirect' => base_url('/tabungan/invoice/' . $setorTabunganId));
                } else {
                    $response = array('success' => false, 'message' => 'Setor Tabungan Gagal, harap hubungi Admin');
                }
            } else {
                $response = array('success' => false, 'message' => 'Tabungan tidak ditemukan');
            }
        } else {
            $response = array('success' => false, 'message' => 'Silahkan input nominal');
        }

        printJson($this, $response);
    }


}