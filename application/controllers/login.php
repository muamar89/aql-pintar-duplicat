<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('UserModel');
        $this->load->model('UserActivityModel');
    }

    public function index()
    {
        $data['title'] = "Login - Forum Bebas Asap Rokok";
        if (!isLoggedIn()) {
            $this->load->view('LoginView', $data);
        } else {
            redirect(base_url('dashboard'));
        }
    }

    public function doLogin()
    {
        $res = "";
        $email = trim($this->input->post('email'));
        $password = trim($this->input->post('password'));

        if ($password) {
            $user = new UserModel();
            $user = $user->getUserByEmailAndPassword($email, $password);

            if ($user == NULL) {
                $res = array('success' => false, 'message' => 'Email / No HP tidak sesuai');
            } else {
                $session_id = uniqid();
                $ip_address = $_SERVER['REMOTE_ADDR'];
                $user_agent = $_SERVER['HTTP_USER_AGENT'];

                $userActivity = new UserActivityModel();
                $userActivity->session_id = $session_id;
                $userActivity->user_id = $user->user_id;
                $userActivity->device_info = $user_agent;
                $userActivity->login_date = now();
                $userActivity->dbInsert($userActivity->fetch());

                $data = array(
                    'session_id' => $session_id,
                    'user_id' => $user->user_id,
                    'member_id' => $user->member_id,
                    'nama' => $user->nama,
                    'email' => $user->email,
                    'no_hp' => $user->no_hp,
                    'avatar' => $user->avatar,
                    'role' => $user->role
                );

                $this->session->set_userdata($data);
                $res = array('success' => true, 'message' => 'Selamat Datang, ' . $user->email, 'redirect' => base_url('/dashboard'));
            }
        }

        printJson($this, $res);
    }

    function logout()
    {
        $userActivity = new UserActivityModel();
        $userActivity->logout(getSessionId());
        $this->session->sess_destroy();
        redirect(base_url('login'));
    }

}
