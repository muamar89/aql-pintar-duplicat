<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Riwayat extends BaseController
{
    var $title = "RIWAYAT KELAS";
    var $cUri = "riwayat";
    var $menuName = "Riwayat";

    function __construct()
    {
        parent::__construct();
        $this->load->model('UserModel');
        $this->load->model('PendaftaranModel');
        $this->load->model('KelasModel');

        // $this->load->model('ProvinsiModel');
        // $this->load->model('ParameterDetailModel');
        // $this->load->model('TabunganModel');
        // $this->load->model('SaldoTabunganModel');
        // $this->load->model('SetorTabunganModel');
    }

    public function index()
    {
        $data['title'] = 'Riwayat Kelas';

        /*$tabungan = new SaldoTabunganModel();
        $data['saldo'] = $tabungan->getSaldo(getSessionUserId());

        $tabungan = new TabunganModel();
        $data['tabungan'] = $tabungan->getByUser(getSessionUserId());*/

        $PendaftaranModel = new PendaftaranModel();
        $data['riwayatKelas'] = $PendaftaranModel->dbGetRows("
        select pendaftaran.*, kelas_name
        from pendaftaran
        inner join kelas on kelas.kelas_id = pendaftaran.kelas_id

        ");

        $this->load->view('HeaderView', $data);
        $this->load->view('RiwayatView', $data);
        $this->load->view('FooterView', $data);
    }
}
