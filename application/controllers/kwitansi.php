<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kwitansi extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('TabunganModel');
        $this->load->model('SaldoTabunganModel');
        $this->load->model('SetorTabunganModel');
        $this->load->model('ProgramUserModel');
        $this->load->model('ProgramModel');
        $this->load->model('UserModel');
    }

    public function index($id)
    {
        include_once APPPATH . '/third_party/mpdf/mpdf.php';

        if ($id) {
            $setorTabunganModel = new SetorTabunganModel();
            $setorTabungan = $setorTabunganModel->getById($id);

            $data['setorTabungan'] = $setorTabungan;

            $tabungan = new TabunganModel();
            $tabungan = $tabungan->getById($setorTabungan->tabungan_id);
            $data['tabungan'] = $tabungan;

            $saldo = new TabunganModel();
            $data['saldo'] = $saldo->getSaldo($setorTabungan->user_id);

            $fullInvoiceNo = explode("/", $setorTabungan->invoice_no);
            $shortInvoiceNo = $fullInvoiceNo[count($fullInvoiceNo)-1];
            $data['invoiceNo'] = $shortInvoiceNo . "/" . $setorTabungan->setoran_ke;

            $setoranAwalModel = new SetorTabunganModel();
            $setoranAwal = $setoranAwalModel->getFirstSetoran($setorTabungan->invoice_no);
            $data['awalSetor'] = date("M Y", strtotime($setoranAwal->updated_date));
            $data['akhirSetor'] = date('M Y', strtotime("+" . ($tabungan->jangka_waktu - 1) . " months", strtotime($setoranAwal->updated_date)));

            /*$user = new UserModel();
            $data['user'] = $user->getById($setorTabungan->user_id);*/

//            $this->load->view('KwitansiView', $data);

            $html = $this->load->view('KwitansiView', $data, true);

            $mPDF = new mPDF('c', 'A5-L', 0,'',5,5,5,0);
            $mPDF->WriteHTML($html);
            $filename = str_replace("/", "_", $setorTabungan->invoice_no . '_' .$setorTabungan->setoran_ke) . '.pdf';
            $filePath = './data/' . $filename;

            if(file_exists($filePath)){
                unlink($filePath);
            }

            $mPDF->Output($filePath, 'I');
        }
    }

    public function program($program_user_id)
    {
        include_once APPPATH . '/third_party/mpdf/mpdf.php';

        if ($program_user_id) {
            $programUserModel = new ProgramUserModel();
            $programUser = $programUserModel->getById($program_user_id);
            $data['programUser'] = $programUser;

            $programModel = new ProgramModel();
            $program = $programModel->getById($programUser->program_id);
            $data['program'] = $program;

            $userModel = new UserModel();
            $data['user'] = $userModel->getById($programUser->user_id);

//            $this->load->view('KwitansiProgramView', $data);

            $html = $this->load->view('KwitansiProgramView', $data, true);

            $mPDF = new mPDF('c', 'A5-L', 0,'',5,5,5,0);
            $mPDF->WriteHTML($html);
            $filename = str_replace(array('/', ' '), '_', $program->nama_program) . "_" . $program_user_id . '.pdf';
            $filePath = './data/' . $filename;

            if(file_exists($filePath)){
                unlink($filePath);
            }

            $mPDF->Output($filePath, 'F');
        }
    }
}
