<?php
defined('BASEPATH') OR exit('No direct script access allowed');
error_reporting(E_ALL);
ini_set('display_errors', 'On');

class Program extends BaseController
{
    var $title = "PROGRAM";
    var $cUri = "program";
    var $menuName = "Program";

    function __construct()
    {
        parent::__construct();
        $this->load->model('UserModel');
        $this->load->model('ProgramModel');
        $this->load->model('ProgramUserModel');
        $this->load->model('ProgramJamaahModel');
        $this->load->model('TabunganModel');
        $this->load->model('SaldoTabunganModel');
        $this->load->model('WebhookModel');
        $this->load->library('BniEnc');
    }

    public function index()
    {
        $data['cUri'] = base_url($this->cUri);
        $data['title'] = $this->title;

        $breadcrumb = "<li class='active'>" . $this->title . "</li>";
        $data['breadcrumb'] = $breadcrumb;

        $program = new ProgramModel();
        $dataTable = $program->dbGetAll();
        $data['dataTable'] = $dataTable;

        $this->load->view('HeaderView', $data);
        $this->load->view('ProgramView', $data);
        $this->load->view('FooterView', $data);
    }

    public function form($id = null)
    {
        $action = ($id == null) ? 'Add' : 'Edit';
        $data['cUri'] = base_url($this->cUri);
        $data['title'] = $this->title;

        $breadcrumb = "<li><a href='" . base_url($this->cUri) . "'>" . $this->title . "</a></li><li class='active'>$action</li>";
        $data['breadcrumb'] = $breadcrumb;

        $program = new ProgramModel();

        if ($id == null) {
            $data['model'] = null;
        } else {
            $data['model'] = $program->getById($id);
        }

        $this->load->view('HeaderView', $data);
        $this->load->view('ProgramFormView', $data);
        $this->load->view('FooterView', $data);
    }

    public function save()
    {
        $id = $this->input->post('program_id');

        $program = new ProgramModel();
        $program->kategori_program = $this->input->post('kategori_program');
        $program->nama_program = $this->input->post('nama_program');
        $program->deskripsi = $this->input->post('deskripsi');
        $program->tanggal_keberangkatan = $this->input->post('tanggal_keberangkatan');
        $program->durasi = $this->input->post('durasi');
        $program->kota_keberangkatan = $this->input->post('kota_keberangkatan');
        $program->kota_tujuan = $this->input->post('kota_tujuan');
        $program->quota = $this->input->post('quota');
        $program->nominal = $this->input->post('nominal');
        $program->banner = $this->input->post('banner');

        if ($id) {
            $program->updated_by = getSessionUserId();
            $program->updated_date = now();
        } else {
            $program->created_by = getSessionUserId();
            $program->created_date = now();
        }

        if ($id) {
            if ($program->dbUpdate($program->fetch(), $id)) {
                $res = array('success' => true, 'message' => 'Program berhasil diupdate', 'redirect' => base_url($this->cUri));
            } else {
                $res = array('success' => false, 'message' => 'Program gagal diupdate');
            }
        } else {
            if ($program->dbInsert($program->fetch())) {
                $res = array('success' => true, 'message' => 'Program berhasil ditambahkan', 'redirect' => base_url($this->cUri));
            } else {
                $res = array('success' => false, 'message' => 'Program gagal ditambahkan');
            }
        }

        printJson($this, $res);
    }


    public function delete($id = null)
    {
        if ($id) {
            $task = new ProgramModel();

            if ($task->delete($id)) {
                $res = array('success' => true, 'message' => $this->title . ' has been deleted', 'redirect' => base_url($this->cUri));
            } else {
                $res = array('success' => false, 'message' => $this->title . ' fail delete');
            }
        } else {
            $res = array('success' => false, 'message' => $this->title . ' fail delete');
        }
        printJson($this, $res);
    }


    public function programlist()
    {
        $kategori_program = $this->input->post('kategori_program');

        $data['cUri'] = base_url($this->cUri);
        $data['title'] = 'Program List';

        $breadcrumb = "<li class='active'>Program List</li>";
        $data['breadcrumb'] = $breadcrumb;

        $query = "select * from program";

        if ($kategori_program) {
            $query .= " where kategori_program = '$kategori_program'";
        }

        $programModel = new ProgramModel();
        $programList = $programModel->dbGetRows($query);
        $data['programList'] = $programList;

        $this->load->view('HeaderView', $data);
        $this->load->view('ProgramListView', $data);
        $this->load->view('FooterView', $data);
    }


    public function programdetail($id = null)
    {
        $data['cUri'] = base_url($this->cUri);
        $data['title'] = $this->title;

        $breadcrumb = "<li class='active'>Program Detail</li>";
        $data['breadcrumb'] = $breadcrumb;


        $programDetail = new ProgramModel();
        $programDetail = $programDetail->getById($id);
        $data['model'] = $programDetail;

        $caraPembyaranList = array();

        $tabungan = new TabunganModel();
        if ($tabungan->getSaldo(getSessionUserId()) >= $programDetail->nominal) {
            array_push($caraPembyaranList, 'TABUNGAN');
        }

        array_push($caraPembyaranList, 'VA BNIS');
        $data['caraPembyaranList'] = $caraPembyaranList;
        $data['saldo'] = $tabungan->getSaldo(getSessionUserId());

        $isRegistered = false;

        $programUser = new ProgramUserModel();
        $programUser = $programUser->getByUser(getSessionUserId());
        if ($programUser != null) {
            $isRegistered = true;
        }

        $data['isRegistered'] = $isRegistered;

        $this->load->view('HeaderView', $data);
        $this->load->view('ProgramDetailView', $data);
        $this->load->view('FooterView', $data);
    }

    public function daftar()
    {
        $isReadySave = true;
        $errorMessage = '';
        $programModel = new ProgramModel();
        $progam = $programModel->getById($this->input->post('program_id'));

        $programUser = new ProgramUserModel();
        $programUser->user_id = getSessionUserId();
        $programUser->program_id = $this->input->post('program_id');
        $programUser->jumlah_keluarga = $this->input->post('jumlah_keluarga');
        $invoice_no = "INV/PROGRAM/" . hexdec(substr(uniqid(), 0, 8));
        $programUser->invoice_no = $invoice_no;

        if ($this->input->post('cara_pembayaran') == 'TABUNGAN') {
            $programUser->status = 'SUDAH BAYAR';
            $saldoTabunganModel = new SaldoTabunganModel();

            $programUser->updated_by = getSessionUserId();
            $programUser->updated_date = now();

            $saldoTabunganModel->insertSaldo(NULL, getSessionUserId(), 0, $progam->nominal);
        } else {
            $programUser->status = 'BELUM BAYAR';

            $programUserExist = new ProgramUserModel();
            $programUserExist = $programUser->getByUser(getSessionUserId());

            if ($programUserExist != null) {
                $suffixVA = intval(substr($programUserExist[0]->va_bnis, 8, 8)) + 1;
            } else {
                $suffixVA = getSessionMemberId() . "01";
            }

            $VA = generateVaNumber($suffixVA);
            $programUser->va_bnis = $VA;

            $user = new UserModel();
            $user = $user->getById(getSessionUserId());

            $response_json = $this->generateInvoice($invoice_no, $user, $VA, $progam->nominal);

            if ($response_json['status'] != '000') {
                $isReadySave = false;
                $errorMessage = $response_json['message'];
            }
        }

        $programUser->nominal = $progam->nominal;
        $programUser->created_by = getSessionUserId();
        $programUser->created_date = now();

        if ($isReadySave) {
            if ($programUser->dbInsert($programUser->fetch())) {
                if ($this->input->post('cara_pembayaran') == 'TABUNGAN') {
                    $userModel = new UserModel();
                    $user = $userModel->getById(getSessionUserId());
                    $fileUrl = generateKwitansiProgram($programUser->getLastcreatedId());
                    $fileName = basename($fileUrl);
                    sendMessageDocument($user->no_hp, $fileUrl, $fileName);
                }

                $res = array('success' => true, 'message' => 'Pendaftaran Berhasil', 'redirect' => base_url($this->cUri) . '/myprogram');
            } else {
                $res = array('success' => false, 'message' => 'Pendaftaran Gagal');
            }
        } else {
            $res = array('success' => false, 'message' => 'Pendaftaran Gagal : ' .$errorMessage);
        }

        printJson($this, $res);
    }

    function generateInvoice($invoice_no, $user, $va, $nominal)
    {
        $data_asli = array(
            'client_id' => BNIS_CLIENT_ID,
            'trx_id' => $invoice_no,
            'trx_amount' => $nominal,
            'billing_type' => 'c',
            'virtual_account' => $va,
            'customer_name' => $user->email,
            'customer_email' => $user->email,
            'customer_phone' => $user->no_hp,
            'type' => 'createBilling'
        );

        $hashed_string = BniEnc::encrypt(
            $data_asli,
            BNIS_CLIENT_ID,
            BNIS_SECRET_KEY
        );

        $data = array(
            'client_id' => BNIS_CLIENT_ID,
            'data' => $hashed_string,
        );

        $response = getContentBnis(BNIS_URL, json_encode($data));
        $response_json = json_decode($response, true);

        if ($response_json['status'] == '000') {
            $data_response = BniEnc::decrypt($response_json['data'], BNIS_CLIENT_ID, BNIS_SECRET_KEY);
            // $data_response['virtual_account']
            $webHook = new WebhookModel();
            $webHook->source = 'va_bnis';
            $webHook->data = json_encode($data_response, true);
            $webHook->dbInsert($webHook->fetch());
        }

        return $response_json;
    }

    public function test()
    {

        $programUser = new ProgramUserModel();
        $programUser = $programUser->getByUser(getSessionUserId());

        $suffixVA = "";
        if ($programUser != null) {
            $newNumber = ((int)$programUser[0]->va_bnis + 1);
            $suffixVA = getSessionMemberId() . $newNumber;
        } else {
            $suffixVA = getSessionMemberId() . "01";
        }

        var_dump($suffixVA);

        //generateVaNumber()
    }

    public function myProgram()
    {
        $data['cUri'] = base_url($this->cUri);
        $data['title'] = 'Program List';

        $breadcrumb = "<li class='active'>My Program</li>";
        $data['breadcrumb'] = $breadcrumb;

//        $query = "select program_user.*, nama_program, tanggal_keberangkatan from program_user
//                inner join program on program_user.program_id = program.program_id
//                where user_id = " . getSessionUserId();

        $query = "select program_user.*, nama_program, tanggal_keberangkatan, terdaftar.total_registered from program_user
                    inner join program on program_user.program_id = program.program_id
                    inner join
                    (select count(program_jamaah.jamaah_id) as total_registered, program_user.program_id, program_user.user_id
                    from program_user left join program_jamaah on program_jamaah.program_user_id = program_user.program_user_id
                    where program_user.user_id = " . getSessionUserId() . ") terdaftar
                    on program_user.user_id = terdaftar.user_id and program_user.program_id = terdaftar.program_id
                    where program_user.user_id = " . getSessionUserId();


        $programUserModel = new ProgramUserModel();
        $data['programUserList'] = $programUserModel->dbGetRows($query);

        $this->load->view('HeaderView', $data);
        $this->load->view('MyProgramView', $data);
        $this->load->view('FooterView', $data);
    }

    public function jamaah($program_user_id)
    {
        $data['cUri'] = base_url($this->cUri);
        $data['title'] = 'Program List';

        $breadcrumb = "<ol class='breadcrumb'>
                <li class='breadcrumb-item'><a href='" . base_url() . "program/myprogram'>My Program</a></li>
                <li class='breadcrumb-item active' aria-current='page'>Program Jamaah</li>
                </ol>";
        $data['breadcrumb'] = $breadcrumb;

        $programUserModel = new ProgramUserModel();
        $programUser = $programUserModel->getById($program_user_id);

        $programModel = new ProgramModel();
        $program = $programModel->getById($programUser->program_id);
//
        $query = "select program_jamaah.*, jamaah.nama_lengkap from program_jamaah
                inner join jamaah on program_jamaah.jamaah_id = jamaah.jamaah_id
                inner join program_user on program_jamaah.program_user_id = program_user.program_user_id
                where program_user.program_user_id =  " . $program_user_id;

        $programJamaahModel = new ProgramJamaahModel();
        $data['dataTable'] = $programJamaahModel->dbGetRows($query);
        $data['program'] = $program;
//
        $this->load->view('HeaderView', $data);
        $this->load->view('ProgramJamaahView', $data);
        $this->load->view('FooterView', $data);
    }

    public function daftarkanprogram()
    {
        $id = $this->input->post('program_id');

        $program = new ProgramModel();
        $program->kategori_program = $this->input->post('kategori_program');
        $program->nama_program = $this->input->post('nama_program');
        $program->deskripsi = $this->input->post('deskripsi');
        $program->tanggal_keberangkatan = $this->input->post('tanggal_keberangkatan');
        $program->durasi = $this->input->post('durasi');
        $program->kota_keberangkatan = $this->input->post('kota_keberangkatan');
        $program->kota_tujuan = $this->input->post('kota_tujuan');
        $program->quota = $this->input->post('quota');
        $program->nominal = $this->input->post('nominal');
        $program->banner = $this->input->post('banner');

        if ($id) {
            $program->updated_by = getSessionUserId();
            $program->updated_date = now();
        } else {
            $program->created_by = getSessionUserId();
            $program->created_date = now();
        }

        if ($id) {
            if ($program->dbUpdate($program->fetch(), $id)) {
                $res = array('success' => true, 'message' => 'Program berhasil diupdate', 'redirect' => base_url($this->cUri));
            } else {
                $res = array('success' => false, 'message' => 'Program gagal diupdate');
            }
        } else {
            if ($program->dbInsert($program->fetch())) {
                $res = array('success' => true, 'message' => 'Program berhasil ditambahkan', 'redirect' => base_url($this->cUri));
            } else {
                $res = array('success' => false, 'message' => 'Program gagal ditambahkan');
            }
        }

        printJson($this, $res);
    }

}