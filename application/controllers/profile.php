<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Profile extends BaseController
{
    var $title = "Profile";
    var $cUri = "profile";

    function __construct()
    {
        parent::__construct();
        // $this->load->model('ProfileModel');
        // $this->load->model('ThreadModel');
        // $this->load->model('PostModel');
        $this->load->model('UserModel');
    }

    public function index()
    {
        $data['cUri'] = base_url($this->cUri);
        $data['title'] = $this->title;

        // $thread = new ThreadModel();
        // $totalThread = $thread->getTotalByCreatedBy(getSessionUserId());

        // $totalPost = $post->getTotalByCreatedBy(getSessionUserId());

        // $data['totalThread'] = $totalThread;
        // $data['totalPost'] = $totalPost;

        $userModel = new userModel();
        $id = getSessionUserId();
        $data['user'] = $userModel->getUser($id);

        $this->load->view('HeaderView', $data);
        $this->load->view('ProfilView', $data);
        $this->load->view('FooterView', $data);
    }

    public function update()
    {
        $user_id = $this->input->post('user_id');
        $no_hp = trim($this->input->post('no_hp'));
        $email = trim($this->input->post('email'));
        $nama = $this->input->post('nama_lengkap');
        $nama_panggilan = $this->input->post('nama_panggilan');
        $jenis_kelamin = $this->input->post('jenis_kelamin');
        $pekerjaan = $this->input->post('pekerjaan');
        $tempat_lahir = $this->input->post('tempat_lahir');
        $tanggal_lahir = $this->input->post('tanggal_lahir');
        $bulan_lahir = $this->input->post('bulan_lahir');
        $tahun_lahir = $this->input->post('tahun_lahir');
        $alamat = $this->input->post('alamat');
        $provinsi_id = $this->input->post('provinsi_id');
        $kota_id = $this->input->post('kota_id');

        $user = new UserModel();
        if ($user_id) {
            // $user->email = $email;
            $user->no_hp = $no_hp;
            $user->email = $email;
            $user->nama = $nama;
            $user->nama_panggilan = $nama_panggilan;
            $user->jenis_kelamin = $jenis_kelamin;
            $user->pekerjaan = $pekerjaan;
            $user->tempat_lahir = $tempat_lahir;
            $user->tanggal_lahir = $tanggal_lahir;
            $user->bulan_lahir = $bulan_lahir;
            $user->tahun_lahir = $tahun_lahir;
            $user->alamat = $alamat;
            $user->provinsi_id = $provinsi_id;
            $user->kota_id = $kota_id;

            // if (!empty($_FILES['file']['name'])) {
            //     $config['upload_path'] = './data/';
            //     $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp';
            //     $config['encrypt_name'] = TRUE;

            //     $this->load->library('upload', $config);
            //     if (!$this->upload->do_upload('file')) {
            //         echo $this->upload->display_errors();
            //     } else {
            //         $data = $this->upload->data();

            //         $config['image_library'] = 'gd2';
            //         $config['source_image'] = './data/' . $data['file_name'];
            //         $config['create_thumb'] = FALSE;
            //         $config['maintain_ratio'] = FALSE;
            //         $config['quality'] = '50%';
            //         $config['new_image'] = './data/' . $data['file_name'];
            //         $this->load->library('image_lib', $config);
            //         $this->image_lib->resize();

            //         $avatar = 'data/' . $data['file_name'];

            //         $user->avatar = $avatar;
            //         $this->session->set_userdata('avatar', $avatar);
            //     }
            // }

            if ($user->dbUpdate($user->fetch(), $user_id)) {
                $res = array('success' => true, 'message' => $this->title . ' berhasil diubah', 'redirect' => base_url($this->cUri));
            } else {
                $res = array('success' => false, 'message' => $this->title . ' gagal diubah');
            }
        } else {
            $res = array('success' => false, 'message' => $this->title . ' gagal diubah');
        }

        printJson($this, $res);
    }

    public function changePassword()
    {
        $user_id = $this->input->post('user_id');
        $current_password = $this->input->post('current_password');
        $new_password = $this->input->post('new_password');
        $repeat_new_password = $this->input->post('repeat_new_password');

        if ($current_password != '' && $new_password != '' && $repeat_new_password != '') {
            $response = array();
            if ($user_id) {
                $user = new UserModel();
                $user = $user->getById($user_id);

                if ($user == NULL) {
                    $response = array('success' => false, 'message' => 'User tidak ditemukan');
                } else {
                    if ($user->password != md5($current_password)) {
                        $response = array('success' => false, 'message' => 'Password Lama tidak sesuai');
                    } else {
                        if (strlen($new_password) < 6) {
                            $response = array('success' => false, 'message' => 'Password Min 6 Character');
                        } else {
                            if ($new_password != $repeat_new_password) {
                                $response = array('success' => false, 'message' => 'Password baru tidak sama');
                            } else {
                                $userUpdate = new UserModel();
                                $userUpdate->password = md5($new_password);
                                $userUpdate->dbUpdate($userUpdate->fetch(), $user->user_id);

                                $response = array('success' => true, 'message' => 'Passwrod Berhasil dirubah');
                            }
                        }
                    }
                }
            }
        } else {
            $response = array('success' => false, 'message' => 'Please fill all data');
        }
        printJson($this, $response);
    }
}
