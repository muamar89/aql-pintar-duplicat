<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kota extends BaseController
{
    var $title = "Kota";
    var $cUri = "kota";
    var $menuName = "Kota";

    function __construct()
    {
        parent::__construct();
        $this->load->model('KotaModel');
    }

    public function getByProvinsiSelect2($provinsiId)
    {
        $kota = new KotaModel();
        $kotaList = $kota->getByProvinsi($provinsiId);

        $data = array();
        foreach ($kotaList as $kota) {
            $tmp = array();
            $tmp['id'] = $kota->id;
            $tmp['text'] = $kota->name;
            array_push($data, $tmp);
        }

        $response = array('success' => true, 'data' => $data);
        printJson($this, $response);
    }

}