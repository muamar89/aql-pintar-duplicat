<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('UserModel');
        $this->load->model('UserActivityModel');
        $this->load->model('ParameterDetailModel');
    }

    public function index()
    {
        $data['title'] = "Register - Forum Bebas Asap Rokok";
        $this->load->view('RegisterView', $data);
    }

    public function isEmailAvailable($email)
    {
        if ($email) {
            $user = new UserModel();
            if ($user->isEmailExist($email)) {
                $res = array('success' => false, 'message' => 'Email sudah terdaftar');
            } else {
                $res = array('success' => true);
            }
        } else {
            $res = array('success' => false, 'message' => 'Email harus diisi');
        }
        printJson($this, $res);
    }

    public function isPhoneNumberAvailable($no_hp)
    {
        if ($no_hp) {
            $user = new UserModel();
            if ($user->isPhoneNumberExist($no_hp)) {
                $res = array('success' => false, 'message' => 'No HP sudah terdaftar');
            } else {
                $res = array('success' => true);
            }
        } else {
            $res = array('success' => false, 'message' => 'No HP harus diisi');
        }
        printJson($this, $res);
    }

    function generateNewMemberId()
    {
        $userModel = new UserModel();
        $lastMemberId = $userModel->getLastMemberId();

        if ($lastMemberId == NULL) {
            $newNumber = "100001";
        } else {
            $lastNumber = "00000" . (((int)substr($lastMemberId, 0, 5)) + 1);
            $newNumber = "1" . substr($lastNumber, -5);
        }

        return $newNumber;
    }


    function doRegister()
    {
        $this->load->library('form_validation');
        $no_hp = trim($this->input->post('no_hp'));
        $nama = trim($this->input->post('nama'));
        $email = trim($this->input->post('email'));
        $password = trim($this->input->post('password'));
        $ulangi_password = trim($this->input->post('ulangi_password'));

        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $response = array('success' => false, 'message' => 'Email tidak valid diisi');
        } else if ($password != $ulangi_password) {
            $response = array('success' => false, 'message' => 'Password tidak sama');
        } else if (strlen($password) < 6) {
            $response = array('success' => false, 'message' => 'Password minimal 6 karakter');
        } else {
            if ($no_hp != '' && $password != '' && $email != '') {
                $checkEmail = new UserModel();
                $checkPhoneNumber = new UserModel();
                if ($checkEmail->isEmailExist($email)) {
                    $response = array('success' => false, 'message' => 'Email sudah terdaftar');
                } else if ($checkPhoneNumber->isPhoneNumberExist($no_hp)) {
                    $response = array('success' => false, 'message' => 'No HP sudah terdaftar');
                } else {
                    $avatarList = array("data/user1.png", "data/user2.png", "data/user3.png", "data/user4.png");

                    $member_id = $this->generateNewMemberId();
                    $userModel = new UserModel();
                    $userModel->nama = $nama;
                    $userModel->no_hp = $no_hp;
                    $userModel->email = $email;
                    $userModel->member_id = $member_id;
                    $userModel->password = md5($password);
                    $userModel->avatar = $avatarList[array_rand($avatarList)];

                    if ($userModel->dbInsert($userModel->fetch())) {
                        $parameterDetail = new ParameterDetailModel();
                        $parameterDetailList = $parameterDetail->getById(10);
                        $message = $parameterDetailList->value;

                        $message = str_replace(array('{member_id}'), array($member_id), $message);
                        $message = str_replace(array('{nama}'), array($nama), $message);
                        $message = str_replace(array('{no_hp}'), array($no_hp), $message);
                        $message = str_replace(array('{email}'), array($email), $message);

                        sendMessageText($no_hp, $message);

                        $response = array('success' => true, 'message' => 'Registrasi Berhasil', 'redirect' => base_url('/login'));
                    } else {
                        $response = array('success' => false, 'message' => 'Registrasi Gagal');
                    }
                }
            } else {
                $response = array('success' => false, 'message' => 'Silahkan lengkapi semua data');
            }
        }

        printJson($this, $response);
    }
}
