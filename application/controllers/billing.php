<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Billing extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('UserModel');
        $this->load->model('ProvinsiModel');
        $this->load->model('ParameterDetailModel');
        $this->load->model('TabunganModel');
        $this->load->model('SaldoTabunganModel');
        $this->load->model('SetorTabunganModel');
        $this->load->model('WebhookModel');

        $this->load->helper('url');
        $this->load->library('BniEnc');
    }

    public function inquiry()
    {
        if (isset($_GET['va'])) {
            $va = $_GET['va'];

            $tabungan = $this->db->query("select * from tabungan
inner join setor_tabungan on tabungan.tabungan_id = setor_tabungan.tabungan_id
where va_bnis = '$va'")->row();

            if ($tabungan != null) {
                $data_asli = array(
                    'client_id' => BNIS_CLIENT_ID,
                    'trx_id' => $tabungan->invoice_no,
                    'type' => 'inquiryBilling'
                );

                $hashed_string = BniEnc::encrypt(
                    $data_asli,
                    BNIS_CLIENT_ID,
                    BNIS_SECRET_KEY
                );

                $data = array(
                    'client_id' => BNIS_CLIENT_ID,
                    'data' => $hashed_string,
                );

                $response = getContentBnis(BNIS_URL, json_encode($data));
                $response_json = json_decode($response, true);

                $data_response = BniEnc::decrypt($response_json['data'], BNIS_CLIENT_ID, BNIS_SECRET_KEY);

                printJson($this, $data_response);
            }

        }
    }

    public function update()
    {
        if (isset($_GET['va'])) {
            $va = $_GET['va'];

            $tabungan = $this->db->query("select * from tabungan
inner join setor_tabungan on tabungan.tabungan_id = setor_tabungan.tabungan_id
where va_bnis = '$va'")->row();

            if ($tabungan != null) {
                $data_asli = array(
                    'client_id' => BNIS_CLIENT_ID,
                    'trx_id' => $tabungan->invoice_no,
                    'trx_amount' => 499999, // minimal dinaikkan dari 100rb ke 499.999
                    'customer_name' => $tabungan->nama_lengkap,
                    'datetime_expired' => '2048-12-31T23:00:00+07:00',
                    'type' => 'updatebilling'
                );

                $hashed_string = BniEnc::encrypt(
                    $data_asli,
                    BNIS_CLIENT_ID,
                    BNIS_SECRET_KEY
                );

                $data = array(
                    'client_id' => BNIS_CLIENT_ID,
                    'data' => $hashed_string,
                );

                $response = getContentBnis(BNIS_URL, json_encode($data));
                $response_json = json_decode($response, true);

                $data_response = BniEnc::decrypt(
                    $response_json['data'],
                    BNIS_CLIENT_ID,
                    BNIS_SECRET_KEY
                );

                $webHook = new WebhookModel();
                $webHook->source = 'va_bnis';
                $webHook->data = json_encode($data_response, true);
                $webHook->dbInsert($webHook->fetch());

                if ($response_json['status'] !== '000') {
                    printJson($this, $response_json);
                } else {
                    $data_response = BniEnc::decrypt($response_json['data'], BNIS_CLIENT_ID, BNIS_SECRET_KEY);
                    printJson($this, $data_response);
                }
            }
        }
    }

    function create()
    {
        if (isset($_GET['va'])) {
            $va = $_GET['va'];

            $tabungan = $this->db->query("select * from tabungan
inner join setor_tabungan on tabungan.tabungan_id = setor_tabungan.tabungan_id
where va_bnis = '$va'")->row();

            if ($tabungan != null) {
                $user = new UserModel();
                $user = $user->getById($tabungan->user_id);

                $trx_id = "INV/" . $tabungan->no_tabungan . "/" . hexdec(substr(uniqid(), 0, 8));
                $data_asli = array(
                    'client_id' => BNIS_CLIENT_ID,
                    'trx_id' => $trx_id,
                    'trx_amount' => 499999,
                    'billing_type' => 'n',
                    'virtual_account' => $tabungan->va_bnis,
                    'customer_name' => $tabungan->nama_lengkap,
                    'customer_email' => $user->email,
                    'customer_phone' => $user->no_hp,
                    'datetime_expired' => '2048-12-31T23:00:00+07:00',
                    'type' => 'createBilling'
                );

                $hashed_string = BniEnc::encrypt(
                    $data_asli,
                    BNIS_CLIENT_ID,
                    BNIS_SECRET_KEY
                );

                $data = array(
                    'client_id' => BNIS_CLIENT_ID,
                    'data' => $hashed_string,
                );

                $response = getContentBnis(BNIS_URL, json_encode($data));
                $response_json = json_decode($response, true);

                if ($response_json['status'] !== '000') {
                    printJson($this, $response_json);
                } else {
                    $data_response = BniEnc::decrypt($response_json['data'], BNIS_CLIENT_ID, BNIS_SECRET_KEY);

                    $setorTabunganUpdateModel = new SetorTabunganModel();
                    $setorTabunganUpdateModel->invoice_no = $trx_id;
                    $setorTabunganUpdateModel->updated_date = now();
                    $setorTabunganUpdateModel->dbUpdateArry($setorTabunganUpdateModel->fetch(), array('user_id' => $tabungan->user_id));

                    printJson($this, $data_response);
                }
            }
        }
    }

    function generateVaNumber($noTabungan)
    {
        return BNIS_PREFIX . BNIS_CLIENT_ID . $noTabungan;
    }


}