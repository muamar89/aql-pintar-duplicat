<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class image extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
    }

    public function upload()
    {
        $config['upload_path'] = './data/';
        $config['allowed_types'] = '*';
//        $config['encrypt_name'] = TRUE;
        $ext = $this->getExtension($_FILES["userfile"]['name']);

        $fileName = preg_replace('/\\.[^.\\s]{3,4}$/', '', $_FILES["userfile"]['name']);

        $config['file_name'] = clean($fileName) . '_' . time() . '.' . $ext;

        $this->load->library('upload', $config);
        if (!$this->upload->do_upload('userfile')) {
            echo $this->upload->display_errors();
        } else {
            $data = $this->upload->data();
//            $result = array('success' => true, 'file' => $data['file_name']);
            $filename = $data['file_name'];
            echo $filename;;
        }
    }

    function getExtension($file)
    {
        $extension = end(explode(".", $file));
        return $extension ? $extension : false;
    }

}