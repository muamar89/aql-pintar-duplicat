<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Vabnis extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('BniEnc');
        $this->load->model('SetorTabunganModel');
        $this->load->model('SaldoTabunganModel');
        $this->load->model('TabunganModel');
        $this->load->model('WebhookModel');
        $this->load->model('UserModel');
        $this->load->model('ParameterDetailModel');
    }

    public function index()
    {
        $content = file_get_contents("php://input");
        $data_json = json_decode($content, true);
        // $this->insertLog($implode);
        if ($data_json['client_id'] === BNIS_CLIENT_ID) {
            $data_asli = BniEnc::decrypt(
                $data_json['data'],
                BNIS_CLIENT_ID,
                BNIS_SECRET_KEY
            );
            // $implode = implode(" - ", $data_asli);

            $amount = $data_asli['payment_amount'];
            $trx_id = $data_asli['trx_id'];

            $webHook = new WebhookModel();
            $webHook->source = 'va_bnis';
            $webHook->data = json_encode($data_asli, true);
            $webHook->dbInsert($webHook->fetch());

            $setorTabunganModel = new SetorTabunganModel();
            $setorTabunganModel = $setorTabunganModel->getLastSetoran($trx_id);

            if ($setorTabunganModel != NULL) {
                if ($setorTabunganModel->setoran_ke == 1 && $setorTabunganModel->status == 'BELUM BAYAR') {
                    $setorTabunganUpdateModel = new SetorTabunganModel();
                    $setorTabunganUpdateModel->status = 'SUDAH BAYAR';
                    $setorTabunganUpdateModel->nominal = $amount;
                    $setorTabunganUpdateModel->updated_date = now();
                    $setorTabunganUpdateModel->dbUpdate($setorTabunganUpdateModel->fetch(), $setorTabunganModel->setor_tabungan_id);

                    $saldoTabunganModel = new SaldoTabunganModel();
                    $saldoTabunganModel->insertSaldo($setorTabunganModel->setor_tabungan_id,
                        $setorTabunganModel->user_id, $amount);

                    // hit API BNIS to update amount of invoice
                    $tabunganModel = new TabunganModel();
                    $tabungan = $tabunganModel->getById($setorTabunganModel->tabungan_id);
                    $this->updateBilling($tabungan, $setorTabunganModel->invoice_no);

                    $invoiceNo = $setorTabunganModel->invoice_no .'/'. $setorTabunganModel->setoran_ke;
                    $setorTabunganId = $setorTabunganModel->setor_tabungan_id;
                } else {
                    $setorTabunganNewModel = new SetorTabunganModel();
                    $setorTabunganNewModel->user_id = $setorTabunganModel->user_id;
                    $setorTabunganNewModel->tabungan_id = $setorTabunganModel->tabungan_id;
                    $setorTabunganNewModel->setoran_ke = $setorTabunganModel->setoran_ke + 1;
                    $setorTabunganNewModel->invoice_no = $setorTabunganModel->invoice_no;
                    $setorTabunganNewModel->nominal = $amount;
                    $setorTabunganNewModel->created_by = $setorTabunganModel->user_id;
                    $setorTabunganNewModel->status = 'SUDAH BAYAR';
                    $setorTabunganNewModel->created_date = now();
                    $setorTabunganNewModel->updated_date = now();
                    $setorTabunganNewModel->dbInsert($setorTabunganNewModel->fetch());

                    $saldoTabunganModel = new SaldoTabunganModel();
                    $saldoTabunganModel->insertSaldo($setorTabunganModel->setor_tabungan_id,
                        $setorTabunganModel->user_id, $amount);

                    $invoiceNo = $setorTabunganModel->invoice_no .'/'. ($setorTabunganModel->setoran_ke + 1);
                    $setorTabunganId = $setorTabunganNewModel->getLastcreatedId();
                }

                $parameterDetail = new ParameterDetailModel();
                $parameterDetailList = $parameterDetail->getById(12);
                $message = $parameterDetailList->value;
                $user = new UserModel();
                $user = $user->getById($setorTabunganModel->user_id);

                $message = str_replace(array('{invoice_no}'), array($invoiceNo), $message);
                sendMessageText($user->no_hp, $message);

                $this->sendKwitansi($setorTabunganId);
            }
        }
    }

    public function updateBilling($tabungan, $invoiceNo)
    {
//        $invoiceNo = 'INV/20000001/20201110191633';
//        $userId = 5;

        $data_asli = array(
            'client_id' => BNIS_CLIENT_ID,
            'trx_id' => $invoiceNo,
            'trx_amount' => 100000,
            'customer_name' => $tabungan->nama_lengkap,
            'type' => 'updatebilling'
        );
        // var_dump($data_asli);
        $hashed_string = BniEnc::encrypt(
            $data_asli,
            BNIS_CLIENT_ID,
            BNIS_SECRET_KEY
        );

        $data = array(
            'client_id' => BNIS_CLIENT_ID,
            'data' => $hashed_string,
        );

        $response = getContentBnis(BNIS_URL, json_encode($data));
        $response_json = json_decode($response, true);

        $data_response = BniEnc::decrypt(
            $response_json['data'],
            BNIS_CLIENT_ID,
            BNIS_SECRET_KEY
        );

        $webHook = new WebhookModel();
        $webHook->source = 'va_bnis';
        $webHook->data = json_encode($data_response, true);
        $webHook->dbInsert($webHook->fetch());

        if ($response_json['status'] !== '000') {
            // handling jika gagal
            var_dump('b');
            var_dump($response_json);
        } else {
            $data_response = BniEnc::decrypt($response_json['data'], BNIS_CLIENT_ID, BNIS_SECRET_KEY);

            var_dump($data_response);
            // $data_response will contains something like this: 
            // array(
            // 	'virtual_account' => 'xxxxx',
            // 	'trx_id' => 'xxx',
            // );
            // var_dump('a');
            // var_dump($data_response);
        }
    }

    public function sendKwitansi($setorTabunganId)
    {
        include_once APPPATH . '/third_party/mpdf/mpdf.php';

        if ($setorTabunganId) {
            $setorTabunganModel = new SetorTabunganModel();
            $setorTabungan = $setorTabunganModel->getById($setorTabunganId);

            $data['setorTabungan'] = $setorTabungan;

            $tabungan = new TabunganModel();
            $tabungan = $tabungan->getById($setorTabungan->tabungan_id);
            $data['tabungan'] = $tabungan;

            $saldo = new TabunganModel();
            $data['saldo'] = $saldo->getSaldo($setorTabungan->user_id);

            $fullInvoiceNo = explode("/", $setorTabungan->invoice_no);
            $shortInvoiceNo = $fullInvoiceNo[count($fullInvoiceNo)-1];
            $data['invoiceNo'] = $shortInvoiceNo . "/" . $setorTabungan->setoran_ke;

            $setoranAwalModel = new SetorTabunganModel();
            $setoranAwal = $setoranAwalModel->getFirstSetoran($setorTabungan->invoice_no);
            $data['awalSetor'] = date("M Y", strtotime($setoranAwal->updated_date));
            $data['akhirSetor'] = date('M Y', strtotime("+" . ($tabungan->jangka_waktu - 1) . " months", strtotime($setoranAwal->updated_date)));


            /*$user = new UserModel();
            $data['user'] = $user->getById($setorTabungan->user_id);*/
//            $this->load->view('KwitansiView', $data);

            $html = $this->load->view('KwitansiView', $data, true);

            $mPDF = new mPDF('c', 'A5-L', 0, '', 5, 5, 5, 0);
            $mPDF->WriteHTML($html);
            $filename = str_replace("/", "_", $shortInvoiceNo . '_' . $setorTabungan->setoran_ke) . '.pdf';
            $filePath = './data/' . $filename;
            $mPDF->Output($filePath, 'F');
//            $mPDF->Output($filePath, 'I'); // diplay pdf

            $fileUrl = base_url() . 'data/' . $filename;

            $user = new UserModel();
            $user = $user->getById($setorTabungan->user_id);

            sendMessageDocument($user->no_hp, $fileUrl, $filename);
        }
    }

    public function test()
    {
        $to = '6285730133016';
//        $parameterDetail = new ParameterDetailModel();
//        $parameterDetailList = $parameterDetail->getById(10);
//        $message = $parameterDetailList->value;
//
//        $id_member = '1010211';
//        $nama = 'Doni Wahyu';
//        $no_hp = '6285730133016';
//        $email = 'doni.wahyu@gmail.com';
//
//        $message = str_replace(array('{id_member}'), array($id_member), $message);
//        $message = str_replace(array('{nama}'), array($nama), $message);
//        $message = str_replace(array('{no_hp}'), array($no_hp), $message);
//        $message = str_replace(array('{email}'), array($email), $message);
//
//        sendMessageDocument($to, 'https://my.safariqu.co.id/data/INV_20000015_20201111072000_1.pdf', 'INV_20000015_20201111072000_1.pdf');
        $this->sendKwitansi(1);
    }
}
