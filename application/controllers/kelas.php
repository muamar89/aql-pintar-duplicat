<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kelas extends BaseController
{
    var $title = "Kelas";
    var $cUri = "kelas";
    var $menuName = "Kelas";

    function __construct()
    {
        parent::__construct();
        $this->load->model('KelasModel');
    }

    public function getAllSelect2()
    {
        $kelas = new KelasModel();
        $kelasList = $kelas->dbGetAll();

        $data = array();
        foreach ($kelasList as $kelas) {
            $tmp = array();
            $tmp['id'] = $kelas->kelas_id;
            $tmp['text'] = $kelas->kelas_name;
            array_push($data, $tmp);
        }

        $response = array('success' => true, 'data' => $data);
        printJson($this, $response);
    }
}
