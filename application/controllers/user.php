<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends BaseController
{
    var $title = "NEW CUSTOMER";
    var $cUri = "user";
    var $menuName = "Customer";

    function __construct()
    {
        parent::__construct();
        $this->load->model('UserModel');
    }


    public function index()
    {
        $data['cUri'] = base_url($this->cUri);
        $data['title'] = $this->title;

        $breadcrumb = "<li class='active'>" . $this->title . "</li>";
        $data['breadcrumb'] = $breadcrumb;

        $this->load->view('HeaderView', $data);
        $this->load->view('UserView', $data);
        $this->load->view('FooterView', $data);
    }

    public function form($userId = null)
    {
        if (!isLoggedIn()) {
            $redirect = urlencode(base_url(uri_string()));
            redirect(base_url() . "login?redirect=$redirect");
        } else {
            $data['cUri'] = base_url($this->cUri);
            $data['title'] = $this->title;

            if ($userId == null) {
                $data['user'] = null;
            } else {
                $user = new PostModel();
                $user = $user->getById($userId);
                $data['user'] = $user;
                $data['content'] = str_replace('<img src="data/', '<img src="' . base_url() . 'data/', $user->content);
            }

            $this->load->view('HeaderView', $data);
            $this->load->view('UserFormView', $data);
            $this->load->view('FooterView', $data);
        }
    }

    public function getAll()
    {
        $pageno = $_GET['pageno'];
        $search_query = $_GET['search_query'];

        $no_of_records_per_page = 50;
        $offset = ($pageno - 1) * $no_of_records_per_page;

        $spam = new UserModel();

        $query = "select * from user where 1 = 1 ";

        if ($search_query != '') {
            $query .= " AND (username LIKE '%$search_query%' OR email LIKE '%$search_query%' OR no_hp LIKE '%$search_query%')";
        }

        $query .= " order by user_id desc LIMIT $offset, $no_of_records_per_page";

        $data = $spam->dbGetRows($query);
        $res = array('data' => $data);
        printJson($this, $res);
    }

    public function banUser()
    {
        $user_id = $this->input->post('user_id');

        if ($user_id) {
            $user = new UserModel();
            $user->status = 'Banned';

            if ($user->dbUpdate($user->fetch(), $user_id)) {
                $res = array('success' => true, 'message' => $this->title . ' berhasil diban', 'redirect' => base_url());
            } else {
                $res = array('success' => false, 'message' => $this->title . ' gagal diban');
            }
        } else {
            $res = array('success' => false, 'message' => $this->title . ' gagal diban');
        }
        printJson($this, $res);
    }

    public function reactivate()
    {
        $user_id = $this->input->post('user_id');

        if ($user_id) {
            $user = new UserModel();
            $user->status = 'Active';

            if ($user->dbUpdate($user->fetch(), $user_id)) {
                $res = array('success' => true, 'message' => $this->title . ' berhasil diaktifkan kembali', 'redirect' => base_url());
            } else {
                $res = array('success' => false, 'message' => $this->title . ' gagal diaktifkan');
            }
        } else {
            $res = array('success' => false, 'message' => $this->title . ' gagal diaktifkan');
        }
        printJson($this, $res);
    }
}