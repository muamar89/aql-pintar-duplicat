<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Setoran extends BaseController
{
    var $title = "SETORAN";
    var $cUri = "setoran";
    var $menuName = "Setoran";

    function __construct()
    {
        parent::__construct();
        $this->load->model('UserModel');
        $this->load->model('ProvinsiModel');
        $this->load->model('ParameterDetailModel');
        $this->load->model('TabunganModel');
        $this->load->model('SaldoTabunganModel');
        $this->load->model('SetorTabunganModel');
    }

    public function index()
    {
        $data['title'] = 'Setoran';

        /*$tabungan = new SaldoTabunganModel();
        $data['saldo'] = $tabungan->getSaldo(getSessionUserId());

        $tabungan = new TabunganModel();
        $data['tabungan'] = $tabungan->getByUser(getSessionUserId());*/

        $setorTabunganModel = new SetorTabunganModel();
        $data['setorTabunganList'] = $setorTabunganModel->dbGetRows("
                        select setor_tabungan.*, nama_lengkap, no_tabungan from setor_tabungan
                        inner join tabungan on tabungan.tabungan_id = setor_tabungan.tabungan_id");

        $this->load->view('HeaderView', $data);
        $this->load->view('SetoranView', $data);
        $this->load->view('FooterView', $data);
    }
}
