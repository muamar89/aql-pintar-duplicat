<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jamaah extends BaseController
{
    var $title = "JAMAAH";
    var $cUri = "jamaah";
    var $menuName = "Jamaah";

    function __construct()
    {
        parent::__construct();
        $this->load->model('UserModel');
        $this->load->model('JamaahModel');
        $this->load->model('ProgramUserModel');
        $this->load->model('ProgramJamaahModel');
    }

    public function registered()
    {
        $data['cUri'] = base_url($this->cUri);
        $data['title'] = $this->title;

        $jamaah = new JamaahModel();
        $dataTable = $jamaah->dbGetAll();
        $data['dataTable'] = $dataTable;

        $this->load->view('HeaderView', $data);
        $this->load->view('JamaahRegisteredView', $data);
        $this->load->view('FooterView', $data);
    }

    public function formregistered($jamaahId = null)
    {
//        $action = ($id == null) ? 'Add' : 'Edit';
        $data['cUri'] = base_url($this->cUri);
        $data['title'] = $this->title;

        $jamaah = new JamaahModel();

        if ($jamaahId == null) {
            $data['model'] = null;
        } else {
            $data['model'] = $jamaah->getById($jamaahId);
        }

        $this->load->view('HeaderView', $data);
        $this->load->view('JamaahFormRegisteredView', $data);
        $this->load->view('FooterView', $data);
    }

    public function index()
    {
        $data['cUri'] = base_url($this->cUri);
        $data['title'] = $this->title;

        $jamaah = new JamaahModel();
        $dataTable = $jamaah->dbGetAll();
        $data['dataTable'] = $dataTable;

        $this->load->view('HeaderView', $data);
        $this->load->view('JamaahView', $data);
        $this->load->view('FooterView', $data);
    }

    public function form($userId = null, $program_user_id = null, $jamaahId = null)
    {
//        $action = ($id == null) ? 'Add' : 'Edit';
        $data['cUri'] = base_url($this->cUri);
        $data['title'] = $this->title;

        $breadcrumb = "<ol class='breadcrumb'>
                <li class='breadcrumb-item'><a href='" . base_url() . "program/myprogram'>My Program</a></li>
                <li class='breadcrumb-item'><a href='" . base_url() . "program/jamaah/" . $program_user_id . "'>Program Jamaah</a></li>
                <li class='breadcrumb-item'><a href='" . base_url() . "jamaah/anggotaKeluarga/" . $program_user_id . "'>Anggota Keluarga</a></li>
                <li class='breadcrumb-item active' aria-current='page'>" . ($jamaahId == NULL ? "Tambah" : "Ubah") . "</li>
                </ol>";
        $data['breadcrumb'] = $breadcrumb;

        $jamaah = new JamaahModel();

        if ($jamaahId == null) {
            $data['model'] = null;
        } else {
            $data['model'] = $jamaah->getById($jamaahId);
        }

        $this->load->view('HeaderView', $data);
        $this->load->view('JamaahFormView', $data);
        $this->load->view('FooterView', $data);
    }

    public function save()
    {
        $id = $this->input->post('jamaah_id');
        $jamaah = new JamaahModel();

        if (isset($_POST['user_id'])) {
            $jamaah->user_id = $this->input->post('user_id');
        }

        $jamaah->nama_lengkap = $this->input->post('nama_lengkap');
        $jamaah->nama_ayah = $this->input->post('nama_ayah');
        $jamaah->tempat_lahir = $this->input->post('tempat_lahir');

        $jamaah->tanggal_lahir = $this->input->post('tahun_lahir') . "-" . $this->input->post('bulan_lahir')
            . "-" . $this->input->post('tanggal_lahir');

        $jamaah->jenis_kelamin = $this->input->post('jenis_kelamin');
        $jamaah->gol_darah = $this->input->post('gol_darah');
        $jamaah->status_perkawinan = $this->input->post('status_perkawinan');
        $jamaah->kewarganegaraan = $this->input->post('kewarganegaraan');
        $jamaah->no_paspor = $this->input->post('no_paspor');

        $jamaah->tanggal_pengeluaran_paspor = $this->input->post('tahun_pengeluaran_paspor') . "-" . $this->input->post('bulan_pengeluaran_paspor')
            . "-" . $this->input->post('tanggal_pengeluaran_paspor');

        $jamaah->kantor_paspor = $this->input->post('kantor_paspor');
        $jamaah->tanggal_kadaluarsa_paspor = $this->input->post('tahun_kadaluarsa_paspor') . "-" . $this->input->post('bulan_kadaluarsa_paspor')
            . "-" . $this->input->post('tanggal_kadaluarsa_paspor');
        $jamaah->alamat_ktp = $this->input->post('alamat_ktp');
        $jamaah->no_hp = $this->input->post('no_hp');
        $jamaah->email = $this->input->post('email');
        $jamaah->pendidikan = $this->input->post('pendidikan');
        $jamaah->berat_badan = $this->input->post('berat_badan');
        $jamaah->tinggi_badan = $this->input->post('tinggi_badan');
        $jamaah->riwayat_penyakit = $this->input->post('riwayat_penyakit');
        $jamaah->penyakit_sedang_diderita = $this->input->post('penyakit_sedang_diderita');
        $jamaah->pekerjaan = $this->input->post('pekerjaan');
        $jamaah->nama_perusahaan = $this->input->post('nama_perusahaan');
        $jamaah->alamat_perusahaan = $this->input->post('alamat_perusahaan');
        $jamaah->nama_keluarga = $this->input->post('nama_keluarga');
        $jamaah->alamat_keluarga = $this->input->post('alamat_keluarga');
        $jamaah->telp_keluarga = $this->input->post('telp_keluarga');
        $jamaah->provinsi_id = $this->input->post('provinsi_id');
        $jamaah->kota_id = $this->input->post('kota_id');
        if ($id) {
            $jamaah->updated_by = getSessionUserId();
            $jamaah->updated_date = now();
        } else {
            $jamaah->created_by = getSessionUserId();
            $jamaah->created_date = now();
        }

        if ($id) {
            if ($jamaah->dbUpdate($jamaah->fetch(), $id)) {
                if (isset($_POST['program_user_id'])) {
                    $program_user_id = $this->input->post('program_user_id');
                    $res = array('success' => true, 'message' => 'Keluarga Berhasil diupdate', 'redirect' => base_url() . 'jamaah/anggotaKeluarga/' . $program_user_id);
                } else {
                    $res = array('success' => true, 'message' => 'Keluarga Berhasil diupdate', 'redirect' => base_url() . 'jamaah/registered');
                }
            } else {
                $res = array('success' => false, 'message' => 'Keluarga Gagal diupdate');
            }
        } else {
            if ($jamaah->dbInsert($jamaah->fetch())) {
                if (isset($_POST['program_user_id'])) {
                    $program_user_id = $this->input->post('program_user_id');
                    $res = array('success' => true, 'message' => 'Keluarga Berhasil ditambahkan', 'redirect' => base_url() . 'jamaah/anggotaKeluarga/' . $program_user_id);
                } else {
                    $res = array('success' => true, 'message' => 'Keluarga Berhasil diupdate', 'redirect' => base_url() . 'jamaah/registered');
                }
            } else {
                $res = array('success' => false, 'message' => 'Keluarga Gagal ditambahkan');
            }
        }

        printJson($this, $res);
    }


    public function delete($id = null)
    {
        if ($id) {
            $task = new JamaahModel();

            if ($task->delete($id)) {
                $res = array('success' => true, 'message' => $this->title . ' has been deleted', 'redirect' => base_url($this->cUri));
            } else {
                $res = array('success' => false, 'message' => $this->title . ' fail delete');
            }
        } else {
            $res = array('success' => false, 'message' => $this->title . ' fail delete');
        }
        printJson($this, $res);
    }

    public function anggotaKeluarga($program_user_id)
    {
        $data['cUri'] = base_url($this->cUri);
        $data['title'] = $this->title;

        $breadcrumb = "<ol class='breadcrumb'>
                <li class='breadcrumb-item'><a href='" . base_url() . "program/myprogram'>My Program</a></li>
                <li class='breadcrumb-item'><a href='" . base_url() . "program/jamaah/" . $program_user_id . "'>Program Jamaah</a></li>
                <li class='breadcrumb-item active' aria-current='page'>Anggota Keluarga</li>
                </ol>";
        $data['breadcrumb'] = $breadcrumb;

        $programUserModel = new ProgramUserModel();
        $programUser = $programUserModel->getById($program_user_id);

        $query = "select * from jamaah where user_id =  " . $programUser->user_id;

        $jamaah = new JamaahModel();
        $data['dataTable'] = $jamaah->dbGetRows($query);

        $this->load->view('HeaderView', $data);
        $this->load->view('JamaahView', $data);
        $this->load->view('FooterView', $data);
    }

    public function daftarkanProgram($program_user_id, $jamaah_id)
    {
        $programJamaahModel = new ProgramJamaahModel();
        $programJamaahModel->program_user_id = $program_user_id;
        $programJamaahModel->jamaah_id = $jamaah_id;
        $programJamaahModel->created_by = getSessionUserId();
        $programJamaahModel->created_date = now();

        $query = $this->db->query("select ifnull(count(program_jamaah.jamaah_id), 0) as total_registered, program_user.jumlah_keluarga
                    from program_user left join program_jamaah on program_jamaah.program_user_id = program_user.program_user_id
                    where program_user.program_user_id = $program_user_id");
        $result = $query->result();

        if ($result[0]->total_registered < $result[0]->jumlah_keluarga) {
            $programJamaahExistModel = new ProgramJamaahModel();
            $programJamaahExistModel = $programJamaahExistModel->getByJamaah($program_user_id, $jamaah_id);

            if ($programJamaahExistModel != NULL) {
                $res = array('success' => false, 'message' => 'Jamaah sudah terdaftar');
            } else {
                if ($programJamaahModel->dbInsert($programJamaahModel->fetch())) {
                    $res = array('success' => true, 'message' => 'Pendaftaran Berhasil', 'redirect' => base_url() . 'program/jamaah/' . $program_user_id);
                } else {
                    $res = array('success' => false, 'message' => 'Pendaftaran Gagal');
                }
            }
        } else {
            $res = array('success' => false, 'message' => 'Jumlah Keluarga Melebihi Batas (' . $result[0]->jumlah_keluarga . ')');
        }

        printJson($this, $res);
    }
}