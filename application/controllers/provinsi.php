<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Provinsi extends BaseController
{
    var $title = "Provinsi";
    var $cUri = "provinsi";
    var $menuName = "Provinsi";

    function __construct()
    {
        parent::__construct();
        $this->load->model('ProvinsiModel');
    }

    public function getAllSelect2()
    {
        $provinsi = new ProvinsiModel();
        $provinsiList = $provinsi->dbGetAll();

        $data = array();
        foreach ($provinsiList as $provinsi) {
            $tmp = array();
            $tmp['id'] = $provinsi->id;
            $tmp['text'] = $provinsi->name;
            array_push($data, $tmp);
        }

        $response = array('success' => true, 'data' => $data);
        printJson($this, $response);
    }

}