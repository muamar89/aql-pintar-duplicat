<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller
{
    var $title = "DASHBOARD";
    var $cUri = "dashboard";
    var $menuName = "Dashboard";

    function __construct()
    {
        parent::__construct();
        $this->load->model('UserModel');
        $this->load->model('TabunganModel');
    }

    public function index($category = NULL)
    {
        /*$data = '';
        $data['title'] = 'Forum Bebas Asap Rokok';
        $data['cUri'] = base_url($this->cUri);
        $data['categoryId'] = $category;

        $category = new CategoryModel();
        $data['categoryList'] = $category->dbGetAll();

        $threadTop = new ThreadModel();
        $data['topThreadList'] = $threadTop->getTopThread();

        $threadTopViewed = new ThreadModel();
        $data['topViewedThreadList'] = $threadTopViewed->getTopViewedThread();

        $threadTotal = new ThreadModel();
        $totalThread = $threadTotal->getTotal();

        $post = new PostModel();
        $totalPost = $post->getTotal();



        $userLast = new UserModel();
        $latestMember = $userLast->getLatestMember();

        $data['totalThreads'] = $totalThread;
        $data['totalPosts'] = $totalPost;
        $data['totalMembers'] = $totalUser;
        $data['latestMember'] = $latestMember[0]->username;

        $threadToday = new ThreadModel();
        $totalThreadToday = $threadToday->getTotal(date('Y-m-d'));

        $postToday = new PostModel();
        $totalPostToday = $postToday->getTotal(date('Y-m-d'));

        $userToday = new UserModel();
        $totalUserToday = $userToday->getTotal(date('Y-m-d'));

        $data['totalThreadsToday'] = $totalThreadToday;
        $data['totalPostsToday'] = $totalPostToday;
        $data['totalMembersToday'] = $totalUserToday;*/

        $data['title'] = 'Dashboard';

        if(getUserRole()=='ADMIN') {

            $user = new UserModel();
            $data['totalUsers'] = $user->getTotal();

            $tabungan = new TabunganModel();
            $data['totalSetoran'] = $tabungan->getSaldo();

            $this->load->view('HeaderView', $data);
            $this->load->view('DashboardAdminView', $data);
            $this->load->view('FooterView', $data);
        }else {
            $tabunganModel = new TabunganModel();
            $data['isTabunganExist'] = $tabunganModel->isTabunganExist(getSessionUserId());

            $this->load->view('HeaderView', $data);
            $this->load->view('DashboardView', $data);
            $this->load->view('FooterView', $data);
        }
    }

    public function getAll()
    {
        $pageno = $_GET['pageno'];
        $category_id = $_GET['category_id'];

        $search_query = '';
        if (isset($_GET['search_query'])) {
            $search_query = $_GET['search_query'];
        }

        $no_of_records_per_page = 10;
        $offset = ($pageno - 1) * $no_of_records_per_page;

        $thread = new ThreadModel();

        $query = "select thread.*, username, avatar from thread
                    inner join user on thread.created_by = user.user_id WHERE is_active = 1 AND 1 = 1 ";

        if ($search_query != '') {
            $query .= " AND (title LIKE '%$search_query%' OR content LIKE '%$search_query%')";
        }

        if ($category_id != '') {
            $query .= " AND category_id = $category_id";
        }

        $query .= " order by thread.thread_id desc LIMIT $offset, $no_of_records_per_page";

        $threadList = $thread->dbGetRows($query);

        foreach ($threadList as $item) {

            $post = new PostModel();
            $totalPost = $post->getTotalByThread($item->thread_id);

            $threadLike = new ThreadLikeModel();
            $totalLike = $threadLike->getTotalByThread($item->thread_id);

            $html = '<a href="' . base_url() . 'thread/detail/' . $item->thread_id . '">
                  <div class="box shadow-sm border rounded bg-white mb-3 osahan-post">
                    <div class="p-3 d-flex align-items-center border-bottom osahan-post-header">
                        <div class="dropdown-list-image mr-3">
                            <img class="rounded-circle" src="' . $item->avatar . '" alt="">
                        </div>
                        <div class="font-weight-bold">
                            <div class="text-truncate">' . $item->title . '</div>
                            <div class="small text-gray-500">' . $item->username . '</div>
                        </div>
                    </div>
                    <div class="p-3 border-bottom osahan-post-body">';

            if ($item->image != '') {
                $html .= '<a href="' . base_url() . 'thread/detail/' . $item->thread_id . '">
                            <img src="' . base_url() . $item->image . '" class="img-fluid fit-image rounded">
                        </a>';
            }

            $html .= ' </div>
                    <div class="p-3 osahan-post-footer">
                        <a style = "cursor:default; font-size: 18px" href="' . base_url() . 'thread/detail/' . $item->thread_id . '" class="mr-3 text-secondary"><i class="feather-thumbs-up text-danger"></i> ' . $totalLike . '</a>
                        <a style = "cursor:default; font-size: 18px" href="' . base_url() . 'thread/detail/' . $item->thread_id . '" class="mr-3 text-secondary"><i class="feather-message-square text-primary"></i> ' . $totalPost . '</a>
                    </div>
                </div>
                </a>';

            echo $html;
        }


        if (count($threadList) >= $no_of_records_per_page) {
            echo '<div class="d-flex align-items-center" style="margin-bottom: 10px;">
                        <div class="mr-auto"></div>
                        <div class="flex-shrink-1">
                           <button type="button" class="btn btn-outline-primary btn-sm" onclick="nextPage(this)">Next Page</button><br>
                        </div>
                     </div>';
        }
    }
}