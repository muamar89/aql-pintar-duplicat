<!DOCTYPE html>
<html>
<head>
    <style>
        td {
            color: #2f368f;
        }

        td {
            /*border: 1px solid lightgrey;*/
            font-size: 20px;
        }

        body {
/*            background: url(*/<?//= base_url() ?>/*assets/img/kwitansi/bg_kwitansi.png);*/
            background-repeat:no-repeat;
            background-position: center;
        }
    </style>
</head>

<body>
<table style="width: 100%" border="0">
    <tr>
        <td style="width:100%;">
            <table style="width:100%;">
                <tr style="height: 50px; ">
                    <td style="width: 15%"><img src="<?= base_url() ?>assets/img/kwitansi/safariqu.png"
                                                style="width: 100%; height: auto;"/></td>
                    <td style="width: 15%"><img src="<?= base_url() ?>assets/img/kwitansi/tabungan_berkah.png"
                                                style="width: 100%; height: auto;"/></td>
                    <td style="width: 35%"><p style="padding-left:20px; font-size: 75px">
                            Jl. Tebet Utara | No. 40, Jakarta Selatan<br>
                            Tlp. <b>021 2283 12218</b> | <b>HP. 0822 1000 2151</b><br>
                            www.safariqu.co.id
                        </p></td>
                    <td style="width: 35%; text-align: center"><p
                            style="font-size: 300px; font-style: italic; font-weight: bold">
                            KWITANSI
                        </p></td>
                </tr>
            </table>
        </td>
    </tr>
</table>

<table style="width: 100%" border="0">
    <tr>
        <td style="width:100%;">
            <table style="width:100%;">
                <tr>
                    <td style="width: 20%"></td>
                    <td style="width: 20%"></td>
                    <td style="width: 20%"></td>
                    <td style="width: 40%;">
                        <table style="width:80%;">
                            <tr>
                                <td style="width:20%; text-align: right;">Nomor</td>
                                <td style="width:5%; text-align: center"> :</td>
                                <td style="border-bottom: 1px solid #2f368f; text-align: right"><?= $programUser->invoice_no ?></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>

    <tr>
        <td style="width:100%; height: 30px;"></td>
    </tr>

    <tr>
        <td style="width:100%; height: 40px">
            <table style="width:100%;">
                <tr>
                    <td style="width:18%;">Telah Terima Dari</td>
                    <td style="width:2%; text-align: center"> :</td>
                    <td style="border-bottom: 1px solid #2f368f; text-align: left"><?= ($user->nama!=NULL)? $user->nama : $user->email ?></td>
                </tr>
            </table>
        </td>
    </tr>

    <tr>
        <td style="width:100%; ; height: 40px">
            <table style="width:100%;">
                <tr>
                    <td style="width:18%;">Program</td>
                    <td style="width:2%; text-align: center"> :</td>
                    <td style="border-bottom: 1px solid #2f368f; text-align: left"><?= $program->nama_program ?></td>
                </tr>
            </table>
        </td>
    </tr>

    <tr>
        <td style="width:100%; ; height: 40px">
            <table style="width:100%;">
                <tr>
                    <td style="width:18%;">Jumlah (Rp)</td>
                    <td style="width:2%; text-align: center"> :</td>
                    <td style="border-bottom: 1px solid #2f368f; text-align: left"><?= thousandSeparator($programUser->nominal) ?></td>
                </tr>
            </table>
        </td>
    </tr>

    <tr>
        <td style="width:100%; ; height: 40px">
            <table style="width:100%;">
                <tr>
                    <td style="width:18%;">Terbilang</td>
                    <td style="width:2%; text-align: center"> :</td>
                    <td style="border-bottom: 1px solid #2f368f; text-align: left"><?= terbilang($programUser->nominal) ?> rupiah</td>
                </tr>
            </table>
        </td>
    </tr>

    <tr>
        <td style="width:100%; ; height: 40px">
            <table style="width:100%;">
                <tr>
                    <td style="width:18%;">Tanggal</td>
                    <td style="width:2%; text-align: center"> :</td>
                    <td style="border-bottom: 1px solid #2f368f; text-align: left"><?= date("d-M-Y", strtotime($programUser->updated_date)) ?></td>
                </tr>
            </table>
        </td>
    </tr>

    <tr>
        <td style="width:100%; ; height: 40px">
            <table style="width:100%;">
                <tr>
                    <td style="width:18%;">Jumlah Keluarga</td>
                    <td style="width:2%; text-align: center"> :</td>
                    <td style="border-bottom: 1px solid #2f368f; text-align: left"><?= $programUser->jumlah_keluarga ?></td>
                </tr>
            </table>
        </td>
    </tr>

    <br>
    <tr>
        <td style="width:100%; ; height: 80px">
            <table style="width:100%;">
                <tr>
                    <td style="width:80%;"></td>
                    <td style="width:20%;"></td>
                    <td style="width:20%; text-align: center; padding-left: 15px;">Diterima Oleh</td>
                </tr>
            </table>
        </td>
    </tr>

    <tr>
        <td style="width:100%;">
            <table style="width:100%;">
                <tr style="height: 60px;">
                    <td style="width: 60%">
                        <img src="<?= base_url() ?>assets/img/kwitansi/semoga_tabungan_berkah.png"
                                                style="width: 80%; height: 85px;"/>
                    </td>
                    <td style="width:20%;"></td>
                    <td style="width: 20%; height: 50px; vertical-align: bottom">

                        <table style="width:100%;">
                            <tr>
                                <td style="width:10%; text-align: right">(</td>
                                <td style="width:80%; border-bottom: 1px solid #2f368f; text-align: center">
                                </td>
                                <td style="width:10%;">)</td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <br>
        </td>
    </tr>
</table>

<table style="width: 100%" border="0">
    <tr>
        <td style="width:100%;">
            <table style="width:100%;">
                <tr style="height: 100px; ">
                    <td style="width: 50%"></td>
                    <td style="width: 50%; ">
                        <img src="<?= base_url() ?>assets/img/kwitansi/footer.png"
                                                style="width: 100%; height: 158px;"/>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

</body>
</html>
