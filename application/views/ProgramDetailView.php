<div class="container-fluid">

    <form id="daftar-form" class="form-signin" method="POST">
        <div class="row">

            <div class="col">

                <div class="card">
                    <div style="
                        background-image: url(<?= base_url(UPLOAD_DATA_DIR . '/' . $model->banner) ?>);
                        height: 500px;
                        background-repeat: no-repeat;
                        background-size: cover;"></div>
                    <!--                    <img class="img-profile" style="height: 350px;" src="-->
                    <? //= base_url(UPLOAD_DATA_DIR . '/' . $model->banner) ?><!--" alt="Card image cap">-->
                    <div class="card-body">
                        <input type="hidden" id="program_id" name="program_id" value="<?= $model->program_id ?>">
                        <h2 class="card-title bold"><?= $model->kategori_program ?></h2>
                        <p class="card-text" style="font-size: 1.4rem; line-height: 1.5;">
                            <small class="text-muted"><?= $model->nama_program ?></small>
                        </p>
                        <p class="card-text" style="font-size: 1.4rem; line-height: 1.5;">
                            <small class="text-muted"><?= $model->deskripsi ?></small>
                        </p>
                        <div class="row">
                            <div class="col-md-6">
                                <table class="table table-striped">
                                    <thead>
                                    <tbody>

                                    <tr>
                                        <td>Tanggal Berangkat</td>
                                        <td><?= $model->tanggal_keberangkatan ?></td>
                                    </tr>
                                    <tr>
                                        <td>Durasi</td>
                                        <td><?= $model->durasi ?> hari</td>
                                    </tr>
                                    <tr>
                                        <td>Kota Keberangkatan</td>
                                        <td><?= $model->kota_keberangkatan ?></td>
                                    </tr>
                                    <tr>
                                        <td>Kota Tujuan</td>
                                        <td><?= $model->kota_tujuan ?></td>
                                    </tr>
                                    <tr>
                                        <td>Quota</td>
                                        <td><?= $model->quota ?></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>
                                            <p class="card-text" style="color: #00aeef;font-size: 1.8rem;">
                                                <small class=""><?= rupiah($model->nominal) ?></small>
                                            </p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Jumlah Keluarga</td>
                                        <td>
                                            <select class="form-control" id="jumlah_keluarga"
                                                    name="jumlah_keluarga" onchange="getTotal(this.value)">
                                                <?
                                                for ($i = 1; $i <= 10; $i++) {
                                                    ?>
                                                    <option value="<?=$i; ?>"><?=$i; ?></option>
                                                    <?
                                                }
                                                ?>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Total</td>
                                        <td>
                                            <p class="card-text" style="color: #00aeef;font-size: 1.8rem;">
                                                <small class="" id="total"><?= rupiah($model->nominal) ?></small>
                                            </p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Cara Pembayaran</td>
                                        <td>
                                            <select class="form-control" id="cara_pembayaran" name="cara_pembayaran">
                                                <?
                                                foreach ($caraPembyaranList as $caraPembyaran) {
                                                    ?>
                                                    <option value="<?=$caraPembyaran?>"><?=$caraPembyaran?></option>
                                                    <?
                                                }
                                                ?>
                                            </select>
                                        </td>
                                    </tr>

                                    </tbody>
                                </table>
                            </div>

                        </div>

                        <hr>

                        <? if(!$isRegistered) { ?>

                            <button type="submit" class="btn btn-primary btn-user ">
                                Daftar
                            </button>

                        <? } ?>
                    </div>
                </div>

            </div>


        </div>
    </form>
</div>



<!-- /.container-fluid -->

<script>
    $('#daftar-form').submit(function (event) {
        event.preventDefault();

        $.blockUI();
        $.ajax({
            type: 'POST',
            url: '<?=base_url()?>program/daftar',
            data: $('#daftar-form').serialize(),
            success: function (res) {
                $.unblockUI();
                if (res.success) {
                    Swal.fire({
                        title: '',
                        text: res.message,
                        icon: 'success',
                        showCancelButton: false,
                        allowEscapeKey: false,
                        allowOutsideClick: false,
                    }).then((result) => {
                        if (result.value) {
                        window.location = res.redirect;
                    }
                })
                } else {
                    Swal.fire(
                        '',
                        res.message,
                        'error'
                    )
                }
            }, error: function (res) {
                $.unblockUI();
                Swal.fire(
                    '',
                    'Connection Error',
                    'error'
                )
            }
        });
    })

    function getTotal(jumlahKeluarga) {
        var saldo = <?=$saldo?>;
        var total = jumlahKeluarga * <?=$model->nominal?>;
        if ($('#cara_pembayaran').val() == 'TABUNGAN') {
            if (saldo >= total) {
                $('#total').html(rupiah(total));
            } else {
                Swal.fire('', 'Saldo Tidak Mencukupi', 'error');
                $('#jumlah_keluarga').val(1);
                $('#total').html(rupiah(<?=$model->nominal?>));
            }
        }else {
            $('#total').html(rupiah(total));
        }
    }
</script>

