<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800"><?= $this->menuName; ?></h1>

    </div>
    <!--<form class="form-inline" id="data">
        <select class="form-control input-small" id="searchField" name="searchField">
            <option value="kategori_program">Ketegori Program</option>

        </select>
        <input type="text" id="searchValue" name="searchValue" class="form-control search-query"
               placeholder="Type to search ...">
    </form>-->
    <br>

    <div class="row">

        <?
        foreach ($programList as $programlist) {

            $num_words = 24;
            $words = array();
            $words = explode(" ", $programlist->deskripsi, $num_words);
            $shown_string = "";

            if (count($words) == 24) {
                $words[23] = " ... ";
            }

            $shown_string = implode(" ", $words);


            ?>
            <div class="col-md-4">

                <div class="card">
                    <img class="img-fluid" style="height: 200px;"
                         src="<?= base_url(UPLOAD_DATA_DIR . '/' . $programlist->banner) ?>" alt="Card image cap">
                    <div class="card-body">
                        <h5 class="card-title bold"><?= $programlist->nama_program ?></h5>
                        <p class="card-text" style="color: #00aeef;font-size: 1.8rem;">
                            <small class=""><?= rupiah($programlist->nominal) ?></small>
                        </p>
                        <a href="<?= base_url() ?>program/programdetail/<?= $programlist->program_id ?>">
                            <button type="submit" class="btn btn-primary btn-user ">
                                Detail
                            </button>
                        </a>
                    </div>
                </div>

            </div>
            <?
        }
        ?>

    </div>

</div>

<!-- /.container-fluid -->


<script>
    var validator = $('#data').validate();

    function doSearch() {
        var dataForm = $('#data').serialize();
        searchValue = dataForm;
        $(grid_selector).jqGrid('setGridParam', {
            url: '<?=$cUri?>/getAll?' + searchValue
        }).trigger('reloadGrid', [{page: 1}]);
    }
</script>