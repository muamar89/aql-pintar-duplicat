<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Setoran</h1>
    </div>

    <div class="card shadow mb-4">
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Nama Lengkap</th>
                            <th>No Tabungan</th>
                            <th>Invoice No</th>
                            <th>Setoran Ke</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>

                        <?
                    foreach ($setorTabunganList as $setorTabungan) {
                        ?>
                        <tr>
                            <td><?= $setorTabungan->nama_lengkap ?></td>
                            <td><?= $setorTabungan->no_tabungan ?></td>
                            <td><?= $setorTabungan->invoice_no ?></td>
                            <td><?= $setorTabungan->setoran_ke ?></td>
                            <td><?= $setorTabungan->status ?></td>
                        </tr>
                        <?
                    }
                    ?>


                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>

<!-- /.container-fluid -->

<script>
    $(document).ready(function() {
        $('#dataTable').DataTable();
    });
</script>