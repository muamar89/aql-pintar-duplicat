<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Invoice</h1>
    </div>

    <!-- Content Row -->
    <div class="row">
        <div class="col-xl-6 col-md-6 mb-6">
            <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">No Invoice</div>
                            <div class="h6 mb-0 font-weight-bold text-gray-800"><?= $setorTabungan->invoice_no ?></div>
                        </div>
                    </div>

                    <hr>

                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Nominal</div>
                            <div class="h6 mb-0 font-weight-bold text-gray-800">
                                Rp.<?= thousandSeparator($setorTabungan->nominal) ?></div>
                        </div>
                    </div>

                    <hr>

                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">VA BNIS</div>
                            <div class="h6 mb-0 font-weight-bold text-gray-800"
                                 id="va_bnis"><?= $tabungan->va_bnis ?></div>
                        </div>
                        <div class="col-auto">
                            <i id="copyButton" class="fas fa-copy fa-2x text-gray-300"></i>
                        </div>
                    </div>

                    <hr>

                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <a href="<?= base_url('tabungan/index') ?>" class="btn btn-primary btn-icon-split">
                    <span class="icon text-white-50">
                      <i class="fas fa-credit-card"></i>
                    </span>
                                <span class="text">Kembali Ke Tabungan</span>
                            </a>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>


</div>
<!-- /.container-fluid -->

<script>
    document.getElementById("copyButton").addEventListener("click", function() {
        copyToClipboard(document.getElementById("va_bnis"));
    });
</script>