<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Profil Saya</h1>
    </div>

    <!-- Content Row -->
    <div class="row">
        <div class="col-xl-12 col-md-12 mb-4">
            <form id="daftar-form" class="form-signin" method="POST">
                <div class="row">
                    <div class="col-sm-6">

                        <div class="form-group row">
                            <div class="col-sm-6">
                                <label for="nama_lengkap">Nama Lengkap</label>
                                <input type="text" id="nama_lengkap" name="nama_lengkap" class="form-control" placeholder="Nama Lengkap" value="<?= getSessionNama() ?>" readonly required>
                                <input type="hidden" id="user_id" name="user_id" class="form-control" placeholder="User Id" value="<?= getSessionUserId() ?>" readonly required>
                            </div>
                            <div class="col-sm-6">
                                <label for="nama_panggilan">Nama Panggilan</label>
                                <input type="text" id="nama_panggilan" name="nama_panggilan" class="form-control" placeholder="Nama Panggilan" value="<?= $user['nama_panggilan']; ?>" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-6">
                                <label for="jenis_kelamin">Jenis Kelamin</label>
                                <select id="jenis_kelamin" name="jenis_kelamin" class="form-control" required>
                                    <?php if ($user['jenis_kelamin'] == "") {
                                        $jenis = 'Pilih Jenis Kelamin';
                                        $jenis_value = '';
                                    } else {
                                        $jenis = $user['jenis_kelamin'];
                                        $jenis_value = $user['jenis_kelamin'];
                                    } ?>
                                    <option value="<?= $jenis_value ?>"><?= $jenis ?></option>
                                    <option value="Laki-laki">Laki-laki</option>
                                    <option value="Perempuan">Perempuan</option>
                                </select>
                            </div>
                            <div class="col-sm-6">
                                <label for="no_hp">No. HP (WA)</label>
                                <input type="text" id="no_hp" name="no_hp" class="form-control" placeholder="contoh : 081234567890" value="<?= getSessionPhoneNumber() ?>" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-6">
                                <label for="email">Email</label>
                                <input type="email" id="email" name="email" class="form-control" placeholder="email@gmail.com" value="<?= getSessionEmail() ?>" required>
                            </div>
                            <div class="col-sm-6">
                                <label for="pekerjaan">Pekerjaan</label>
                                <input type="text" id="pekerjaan" name="pekerjaan" class="form-control" placeholder="Pekerjaan" value="<?= $user['pekerjaan'] ?>" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-4">
                                <label for="tempat_lahir">Tempat Lahir</label>
                                <input type="text" id="tempat_lahir" name="tempat_lahir" class="form-control" placeholder="Tempat Lahir" value="<?= $user['tempat_lahir'] ?>" required>
                            </div>
                            <div class="col-sm-2">
                                <label for="tanggal_lahir">Tanggal</label>
                                <select class="form-control select2" id="tanggal_lahir" name="tanggal_lahir" required>
                                    <option value="">0</option>
                                    <?
                                    for ($i = 1; $i <= 31; $i++) {
                                        if($user['tanggal_lahir'] == $i){
                                            $slct = 'selected';
                                        } else {
                                            $slct = '';
                                        }
                                        ?>
                                    <option value="<?= substr("0" . $i, -2); ?>" <?= $slct; ?>><?= $i; ?></option>
                                    <?
                                    }
                                    ?>
                                </select>


                            </div>

                            <div class="col-sm-3">
                                <label for="bulan_lahir">Bulan</label>
                                <select class="form-control select2" id="bulan_lahir" name="bulan_lahir" required>
                                    <option value="">Pilih Bulan</option>
                                    <?
                                    $bulan=array("Jan", "Feb", "Mar", "Apr", "Mei", "Jun", "Jul", "Agu", "Sep", "Okt", "Nov", "Des");
                                    $bulanNo=array("01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12");
                                    for ($i = 0; $i < count($bulan); $i++) {
                                        if($user['bulan_lahir'] == $bulanNo[$i]){
                                            $slct = 'selected';
                                        } else {
                                            $slct = '';
                                        }
                                        ?>
                                    <option value="<?= $bulanNo[$i] ?>" <?= $slct; ?>><?= $bulan[$i] ?></option>
                                    <?
                                    }
                                    ?>
                                </select>
                            </div>

                            <div class="col-sm-3">
                                <label for="tahun_lahir">Tahun</label>
                                <select class="form-control select2" id="tahun_lahir" name="tahun_lahir" required>
                                    <option value="">Pilih Tahun</option>
                                    <?
                                    for ($i = 1960; $i <= 2020; $i++) {
                                        if($user['tahun_lahir'] == $i){
                                            $slct = 'selected';
                                        } else {
                                            $slct = '';
                                        }
                                        ?>
                                    <option value="<?= $i; ?>" <?= $slct; ?>><?= $i; ?></option>
                                    <?
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-6">
                                <label for="provinsi_id">Provinsi</label>
                                <select class="form-control select2" id="provinsi_id" name="provinsi_id" required></select>
                            </div>
                            <div class="col-sm-6">
                                <label for="kota_id">Kota</label>
                                <select class="form-control select2" id="kota_id" name="kota_id" required></select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="alamat">Alamat sesuai KTP</label>
                            <textarea type="text" id="alamat" name="alamat" class="form-control" placeholder="Alamat sesuai KTP" required><?= $user['alamat'] ?></textarea>
                        </div>

                        <!-- <div class="form-group row">
                            <div class="col-sm-6">
                                <label for="target_tabungan">Target Tabungan</label>
                                <select class="form-control select2" id="target_tabungan" name="target_tabungan" required></select>
                            </div>
                            <div class="col-sm-6">
                                <label for="jangka_waktu">Jangka Waktu</label>
                                <select class="form-control select2" id="jangka_waktu" name="jangka_waktu" required></select>
                            </div>
                        </div> -->

                        <!-- <div class="form-group">
                            <label for="jumlah_keluarga">Jumlah Keluarga (Peserta Tabungan Berkah)</label>
                            <select class="form-control select2" id="jumlah_keluarga" name="jumlah_keluarga" required></select>
                        </div> -->

                        <!-- <div class="form-group row" id="div_estimasi_cicilan" style="display: none">

                            <div class="col-xl-6 col-md-6 mb-4">
                                <div class="card border-left-primary shadow h-100 py-2">
                                    <div class="card-body">
                                        <div class="row no-gutters align-items-center">
                                            <div class="col mr-2">
                                                <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                                    Estimasi Setoran / Bulan
                                                </div>
                                                <div class="h5 mb-0 font-weight-bold text-gray-800" id="estimasi_cicilan">Rp.
                                                </div>
                                            </div>
                                            <div class="col-auto">
                                                <i class="fas fa-money-check fa-2x text-gray-300"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> -->

                        <div class="form-group row">
                            <div class="col-sm-6">
                                <button type="submit" class="btn btn-block btn-primary">Simpan Profil Saya</button>
                            </div>

                        </div>

                    </div>
                </div>
            </form>
        </div>
    </div>


</div>
<!-- /.container-fluid -->

<script>
    $(document).ready(function() {
        $('#btn-daftar').click(function() {
            $('#daftar-form').submit();
        });

        $('#daftar-form').submit(function(event) {
            event.preventDefault();

            $.blockUI();
            $.ajax({
                type: 'POST',
                url: '<?= base_url() ?>profile/update',
                data: $('#daftar-form').serialize(),
                success: function(res) {
                    $.unblockUI();
                    if (res.success) {
                        Swal.fire({
                            title: '',
                            text: res.message,
                            icon: 'success',
                            showCancelButton: false,
                            allowEscapeKey: false,
                            allowOutsideClick: false,
                        }).then((result) => {
                            if (result.value) {
                                window.location = res.redirect;
                            }
                        })
                    } else {
                        Swal.fire(
                            '',
                            res.message,
                            'error'
                        )
                    }
                },
                error: function(res) {
                    $.unblockUI();
                    Swal.fire(
                        '',
                        'Connection Error',
                        'error'
                    )
                }
            });
        })

        $.ajax({
            url: "<?= base_url('provinsi/getAllSelect2') ?>",
            dataType: 'json',
            type: 'GET',
            cache: false
        }).then(function(result) {
            $('#provinsi_id').select2({
                data: result.data
            }).on('change', function() {
                var data = $("#provinsi_id option:selected").text();
                if (data != '') {
                    getKota();
                }
            });

            $("#provinsi_id").val("")
            $("#provinsi_id").trigger("change");
        });

        $.ajax({
            url: "<?= base_url('tabungan/targettabungan') ?>",
            dataType: 'json',
            type: 'GET',
            cache: false
        }).then(function(result) {
            $('#target_tabungan').select2({
                data: result.data
            }).on('change', function() {
                var data = $("#target_tabungan option:selected").text();
                estimasiCicilan();
            });

            $("#target_tabungan").val("")
            $("#target_tabungan").trigger("change");

        });

        $.ajax({
            url: "<?= base_url('tabungan/jangkawaktu') ?>",
            dataType: 'json',
            type: 'GET',
            cache: false
        }).then(function(result) {
            $('#jangka_waktu').select2({
                data: result.data
            }).on('change', function() {
                var data = $("#jangka_waktu option:selected").text();
                estimasiCicilan();
            });

            $("#jangka_waktu").val("")
            $("#jangka_waktu").trigger("change");
        });

        $.ajax({
            url: "<?= base_url('tabungan/jumlahkeluarga') ?>",
            dataType: 'json',
            type: 'GET',
            cache: false
        }).then(function(result) {
            $('#jumlah_keluarga').select2({
                data: result.data
            }).on('change', function() {
                var data = $("#jumlah_keluarga option:selected").text();
                estimasiCicilan();
            });

            $("#jumlah_keluarga").val("")
            $("#jumlah_keluarga").trigger("change");
        });

    });

    function estimasiCicilan() {
        if ($("#target_tabungan option:selected").text() != '' && $("#jangka_waktu option:selected").text() != '' &&
            $("#jumlah_keluarga option:selected").text() != '') {

            var totalEstimasi = ($('#target_tabungan').val() * $('#jumlah_keluarga').val()) / $('#jangka_waktu').val();
            $('#estimasi_cicilan').html("Rp." + thousandSeparator(parseInt(totalEstimasi)));
            $('#div_estimasi_cicilan').fadeIn();
        }
    }

    function thousandSeparator(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
    }

    function getKota() {
        $.ajax({
            url: "<?= base_url() ?>kota/getByProvinsiSelect2/" + $('#provinsi_id').val(),
            dataType: 'json',
            type: 'GET',
            cache: false
        }).then(function(result) {
            $('#kota_id').empty().select2({
                data: result.data
            });

            $("#kota_id").val("")
            $("#kota_id").trigger("change");
        });
    }
</script>