<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">My Program</h1>
    </div>


    <div class="row">
        <?
        if ($programUserList != null) {
            foreach ($programUserList as $programUser) {
                ?>
                <div class="col-md-12">
                    <div
                        class="card border-left-<?= $programUser->status == "SUDAH BAYAR" ? "success" : "danger" ?> shadow h-100 py-2">
                        <div class="card-body">
                            <div class="row no-gutters align-items-center">
                                <div class="col">
                                    <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Program</div>
                                    <div
                                        class="h7 mb-0 font-weight-bold text-gray-800"><?= $programUser->nama_program ?></div>
                                </div>

                                <div class="col">
                                    <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Tanggal
                                        Berangkat
                                    </div>
                                    <div
                                        class="h7 mb-0 font-weight-bold text-gray-800"><?= readableDate($programUser->tanggal_keberangkatan) ?></div>
                                </div>

                                <div class="col">
                                    <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Status</div>
                                    <div
                                        class="h7 mb-0 font-weight-bold text-gray-800"><?= $programUser->status ?></div>
                                </div>


                                <?
                                if ($programUser->status == "BELUM BAYAR") {
                                    ?>
                                    <div class="col">
                                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">VA BNIS
                                        </div>
                                        <div class="h7 mb-0 font-weight-bold text-gray-800"><?= $programUser->va_bnis ?>
                                            <i style="cursor: pointer" onclick="copyVa(this)"
                                               class="fas fa-copy text-primary"></i>
                                        </div>
                                    </div>
                                    <?
                                    echo $this->db->last_query();
                                } ?>

                                <?
                                if ($programUser->status == "SUDAH BAYAR") {
                                    ?>
                                    <div class="col">
                                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Anggota
                                            Keluarga
                                        </div>
                                        <span
                                            style="font-size: 0.8rem">Jumlah Keluarga (<?= $programUser->jumlah_keluarga ?>
                                            ) <br>Terdaftar (<?= $programUser->total_registered ?>)</span>
                                        <i style="cursor: pointer"
                                           onclick="programJamaah(<?= $programUser->program_user_id ?>)"
                                           class="fas fa-users text-primary" title="Pendaftaran Keluarga"></i>
                                    </div>
                                    <?
                                } ?>


                            </div>

                        </div>
                    </div>
                </div>
                <?
            }
        }
        ?>

    </div>

</div>

<!-- /.container-fluid -->


<script>
    var validator = $('#data').validate();

    function doSearch() {
        var dataForm = $('#data').serialize();
        searchValue = dataForm;
        $(grid_selector).jqGrid('setGridParam', {
            url: '<?=$cUri?>/getAll?' + searchValue
        }).trigger('reloadGrid', [{page: 1}]);
    }

    function copyVa(obj) {
        copyToClipboard($(obj).parent());
    }

    function programJamaah(program_user_id) {
        window.location = '<?=base_url()?>program/jamaah/' + program_user_id
    }
</script>