<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Top Up Tabungan</h1>
    </div>

    <!-- Content Row -->
    <div class="row">
        <div class="col-xl-12 col-md-12 mb-4">
            <form id="setor-form" class="form-signin" method="POST">
                <div class="row">
                    <div class="col-sm-6">

                        <div class="form-group row">
                            <div class="col-sm-12">
                                <label for="nama_lengkap">Nominal</label>
                                <input type="number" id="nominal" name="nominal" min="<?=$minNominal?>"
                                       class="form-control" placeholder="<?=$placeHolderNonimal?>" required>
                            </div>
                        </div>


                        <div class="form-group row">
                            <div class="col-sm-6">
                                <button type="submit" class="btn btn-block btn-primary">Setor</button>
                            </div>

                        </div>

                    </div>
                </div>
            </form>
        </div>
    </div>


</div>
<!-- /.container-fluid -->

<script>
    $(document).ready(function () {
        $('#btn-setor').click(function(){
            $('#setor-form').submit();
        });

        $('#setor-form').submit(function (event) {
            event.preventDefault();

            $.blockUI();
            $.ajax({
                type: 'POST',
                url: '<?=base_url()?>tabungan/doSetor',
                data: $('#setor-form').serialize(),
                success: function (res) {
                    $.unblockUI();
                    if (res.success) {
                        window.location = res.redirect;
                    } else {
                        Swal.fire(
                            '',
                            res.message,
                            'error'
                        )
                    }
                }, error: function (res) {
                    $.unblockUI();
                    Swal.fire(
                        '',
                        'Connection Error',
                        'error'
                    )
                }
            });
        })

        $.ajax({
            url: "<?=base_url('provinsi/getAllSelect2')?>",
            dataType: 'json', type: 'GET', cache: false
        }).then(function (result) {
            $('#provinsi_id').select2({
                data: result.data
            }).on('change', function() {
                var data = $("#provinsi_id option:selected").text();
                if(data!='') {
                    getKota();
                }
            });

            $("#provinsi_id").val("")
            $("#provinsi_id").trigger("change");
        });

        $.ajax({
            url: "<?=base_url('tabungan/targettabungan')?>",
            dataType: 'json', type: 'GET', cache: false
        }).then(function (result) {
            $('#target_tabungan').select2({
                data: result.data
            }).on('change', function() {
                var data = $("#target_tabungan option:selected").text();
                estimasiCicilan();
            });

            $("#target_tabungan").val("")
            $("#target_tabungan").trigger("change");

        });

        $.ajax({
            url: "<?=base_url('tabungan/jangkawaktu')?>",
            dataType: 'json', type: 'GET', cache: false
        }).then(function (result) {
            $('#jangka_waktu').select2({
                data: result.data
            }).on('change', function() {
                var data = $("#jangka_waktu option:selected").text();
                estimasiCicilan();
            });

            $("#jangka_waktu").val("")
            $("#jangka_waktu").trigger("change");
        });

        $.ajax({
            url: "<?=base_url('tabungan/jumlahkeluarga')?>",
            dataType: 'json', type: 'GET', cache: false
        }).then(function (result) {
            $('#jumlah_keluarga').select2({
                data: result.data
            }).on('change', function() {
                var data = $("#jumlah_keluarga option:selected").text();
                estimasiCicilan();
            });

            $("#jumlah_keluarga").val("")
            $("#jumlah_keluarga").trigger("change");
        });

    });

    function estimasiCicilan() {
        if ($("#target_tabungan option:selected").text() != '' && $("#jangka_waktu option:selected").text() != ''
            && $("#jumlah_keluarga option:selected").text() != '') {

            var totalEstimasi = ($('#target_tabungan').val() * $('#jumlah_keluarga').val()) / $('#jangka_waktu').val();
            $('#estimasi_cicilan').html("Rp." + thousandSeparator(parseInt(totalEstimasi)));
            $('#div_estimasi_cicilan').fadeIn();
        }
    }

    function thousandSeparator(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
    }

    function getKota() {
        $.ajax({
            url: "<?=base_url()?>kota/getByProvinsiSelect2/" + $('#provinsi_id').val(),
            dataType: 'json', type: 'GET', cache: false
        }).then(function (result) {
            $('#kota_id').empty().select2({
                data: result.data
            });

            $("#kota_id").val("")
            $("#kota_id").trigger("change");
        });
    }
</script>