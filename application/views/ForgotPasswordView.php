<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" href="<?= base_url() ?>assets/img/fav.png">
    <title>Nusantara Tanpa Asap Rokok - <?= $title ?></title>
    <!-- Slick Slider -->
    <link rel="stylesheet" type="text/css"
          href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/themes/smoothness/jquery-ui.css"/>
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/vendor/slick/slick.min.css"/>
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/vendor/slick/slick-theme.min.css"/>
    <!-- Feather Icon-->
    <link href="<?= base_url() ?>assets/vendor/icons/feather.css" rel="stylesheet" type="text/css">
    <!-- Bootstrap core CSS -->
    <link href="<?= base_url() ?>assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="<?= base_url() ?>assets/css/style.css" rel="stylesheet">
</head>
<body>

<div class="bg-white">
    <div class="container">
        <div class="row justify-content-center align-items-center d-flex vh-100">
            <div class="col-md-4 mx-auto">
                <div class="osahan-login py-4">
                    <div class="text-center mb-4">
                        <a href="<?=base_url()?>"><img src="<?= base_url() ?>assets/img/logo.svg"alt=""></a>
                        <h5 class="font-weight-bold mt-3">Lupa Password</h5>
                    </div>
                    <form id="forgot-password-form">
                        <div class="form-group">
                            <label class="mb-1">Email</label>
                            <div class="position-relative icon-form-control">
                                <i class="feather-mail position-absolute"></i>
                                <input type="email" id="email" name="email" class="form-control" required>
                            </div>
                        </div>
                        <button class="btn btn-primary btn-block text-uppercase" type="submit"> Reset</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Bootstrap core JavaScript -->
<script src="<?= base_url() ?>assets/vendor/jquery/jquery.min.js"></script>
<script src="<?= base_url() ?>assets/vendor/jquery/jquery.blockUI.js"></script>
<script src="<?= base_url() ?>assets/vendor/jquery/jquery-ui.min.js"></script>
<script src="<?= base_url() ?>assets/vendor/jquery/jquery.validate.min.js"></script>
<script src="<?= base_url() ?>assets/vendor/bootbox/bootbox.all.min.js"></script>
<script src="<?= base_url() ?>assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- Custom scripts for all pages-->
<script src="<?= base_url() ?>assets/js/osahan.js"></script>
<script src="<?= base_url() ?>assets/js/app.js"></script>

<script>
//    $('#email').val('doni.wahyu@gmail.com');

    $('#forgot-password-form').submit(function (event) {
        event.preventDefault();

        $.blockUI();
        $.ajax({
            type: 'POST',
            url: '<?=base_url()?>forgotPassword/doForgotPassword',
            data: $('#forgot-password-form').serialize(),
            success: function (res) {
                $.unblockUI();
                if (!res.success) {
                    message(res.message)
                } else {
                    bootbox.alert({
                        message: res.message,
                        centerVertical: true,
                        callback: function () {
                            redirect(res.redirect)
                        }
                    });
                }
            }, error: function (res) {
                $.unblockUI();
            }
        });
    });
</script>

</body>
</html>

 