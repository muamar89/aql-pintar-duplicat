<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Riwayat Kelas</h1>
    </div>

    <div class="card shadow mb-4">
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Kelas</th>
                            <th>Lokasi</th>
                            <th>Invoice No</th>
                            <th>Setoran Ke</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>

                        <?
                        $no = 1; 
                        foreach ($riwayatKelas as $riwayat) {
                        ?>
                        <tr>
                            <td><?= $no ?></td>
                            <td><?= $riwayat->kelas_name ?></td>
                            <td><?= $riwayat->lokasi ?></td>
                            <td><?= $riwayat->kelas_name ?></td>
                            <td><?= $riwayat->kelas_name ?></td>
                            <td><?= $riwayat->kelas_name ?></td>
                        </tr>
                        <?
                        $no++; } 
                        ?>

                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>

<!-- /.container-fluid -->

<script>
    $(document).ready(function() {
        $('#dataTable').DataTable();
    });
</script>