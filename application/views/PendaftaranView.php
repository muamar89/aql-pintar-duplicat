<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Pendaftaran</h1>
    </div>

    <!-- Content Row -->
    <div class="row">
        <div class="col-xl-12 col-md-12 mb-4">
            <form id="daftar-form" class="form-signin" method="POST" enctype="multipart/form-data">
                <!-- <form action="<?= base_url() ?>pendaftaran/savePendaftaran" id="daftar-form" class="form-signin" method="POST" enctype="multipart/form-data"> -->
                <div class="row">
                    <div class="col-sm-6">

                        <div class="form-group row">
                            <!-- <div class="col-sm-6">
                                <label for="nama_lengkap">Nama Lengkap</label>
                                <input type="text" id="nama_lengkap" name="nama_lengkap" class="form-control" placeholder="Nama Lengkap" value="" required>
                            </div> -->
                            <div class="col-sm-6">
                                <label for="lokasi">Lokasi</label>
                                <select id="lokasi" name="lokasi" class="form-control select2" required>
                                    <option value="">Pilih Lokasi</option>
                                    <option value="Dalam Negeri">Dalam Negeri</option>
                                    <option value="Luar Negeri">Luar Negeri</option>
                                </select>
                            </div>

                        </div>

                        <div class="form-group row">
                            <div class="col-sm-6">
                                <label for="kelas_id">Pilih Kelas</label>
                                <select class="form-control select2" id="kelas_id" name="kelas_id" required></select>
                            </div>
                        </div>
                        <!--<div class="form-group row">
                            <div class="col-sm-12">
                                <label for="upload_file">Upload rekaman suara bacaan Qurannya Surat Al-Hajj ayat 5 <b>(khusus baru & cuti)</b></label>
                                <input type="file" id="upload_file" name="upload_file" class="" placeholder="" value="">
                            </div>
                        </div>-->


                        <div class="form-group row">
                            <div class="col-sm-6">
                                <label for="record">Rekaman suara bacaan Qurannya Surat Al-Hajj ayat 5 <b>(khusus baru & cuti)</b></label>
                                <!--                                <input type="file" id="upload_file" name="upload_file" class="" placeholder="" value="">-->

                                <input type="button" id="recordButton" class="btn btn-warning" value="Mulai Rekam">

                                <input type="hidden" id="file_rekaman" name="file_rekaman">
                                <img id="rekam_gif" src="assets/img/recording-gif-6.gif" style="width: 200px; height: 150px; display: none" />
                                <div id="hasil_rekaman"></div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-6">
                                <button type="submit" class="btn btn-block btn-primary">Daftar</button>
                            </div>

                        </div>

                    </div>
                </div>
            </form>
        </div>
    </div>


</div>
<!-- /.container-fluid -->

<script>
    $(document).ready(function() {
        $('#btn-daftar').click(function() {
            $('#daftar-form').submit();
        });

        $('#daftar-form').submit(function(event) {
            event.preventDefault();

            $.blockUI();
            $.ajax({
                type: 'POST',
                url: '<?= base_url() ?>pendaftaran/savePendaftaran',
                data: $('#daftar-form').serialize(),
                // data: new FormData(this),
                // processData: false,
                // contentType: false,
                // cache: false,
                // async: false,
                success: function(res) {
                    $.unblockUI();
                    if (res.success) {
                        Swal.fire({
                            title: '',
                            text: res.message,
                            icon: 'success',
                            showCancelButton: false,
                            allowEscapeKey: false,
                            allowOutsideClick: false,
                        }).then((result) => {
                            if (result.value) {
                                window.location = res.redirect;
                            }
                        })
                    } else {
                        Swal.fire(
                            '',
                            res.message,
                            'error'
                        )
                    }
                },
                error: function(res) {
                    $.unblockUI();
                    Swal.fire(
                        '',
                        'Connection Error',
                        'error'
                    )
                }
            });
        })

        $.ajax({
            url: "<?= base_url('kelas/getAllSelect2') ?>",
            dataType: 'json',
            type: 'GET',
            cache: false
        }).then(function(result) {
            $('#kelas_id').select2({
                data: result.data
            }).on('change', function() {
                var data = $("#kelas_id option:selected").text();
                // if (data != '') {
                //     getKota();
                // }
            });

            $("#kelas_id").val("")
            $("#kelas_id").trigger("change");
        });
    });

    function getKota() {
        $.ajax({
            url: "<?= base_url() ?>kota/getByProvinsiSelect2/" + $('#provinsi_id').val(),
            dataType: 'json',
            type: 'GET',
            cache: false
        }).then(function(result) {
            $('#kota_id').empty().select2({
                data: result.data
            });

            $("#kota_id").val("")
            $("#kota_id").trigger("change");
        });
    }
</script>

<script>
    var recorder, gumStream;
    var recordButton = document.getElementById("recordButton");
    var hasilRekaman = document.getElementById("hasil_rekaman");
    var rekamGif = document.getElementById("rekam_gif");
    var fileRekaman = document.getElementById("file_rekaman");
    recordButton.addEventListener("click", toggleRecording);

    function toggleRecording() {
        if (recorder && recorder.state == "recording") {
            recordButton.value = 'Mulai Rekam';
            rekamGif.style.display = "none";
            recorder.stop();
            gumStream.getAudioTracks()[0].stop();
        } else {
            recordButton.value = 'Berhenti';
            rekamGif.style.display = "block";
            navigator.mediaDevices.getUserMedia({
                audio: true
            }).then(function(stream) {
                gumStream = stream;
                recorder = new MediaRecorder(stream);
                recorder.ondataavailable = function(e) {


                    var reader = new window.FileReader();
                    reader.readAsDataURL(e.data);
                    reader.onloadend = function() {
                        base64data = reader.result;
                        fileRekaman.value = base64data;
                        console.log(base64data)
                    }

                    var url = URL.createObjectURL(e.data);
                    var preview = document.createElement('audio');
                    preview.controls = true;
                    preview.src = url;
                    while (hasilRekaman.hasChildNodes()) {
                        hasilRekaman.removeChild(hasilRekaman.firstChild);
                    }
                    hasilRekaman.appendChild(preview);
                };
                recorder.start();
            });
        }
    }
</script>