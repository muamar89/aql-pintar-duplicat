<style>
    .upload-area {
        background-color: #cacaca;
        width: 100%;
        height: 180px;
        border: 2px solid lightgray;
        border-radius: 3px;
        margin: 0 auto;
        text-align: center;
        overflow: hidden;
    }

    .upload-area:hover {
        cursor: pointer;
    }

    .upload-area h1 {
        text-align: center;
        font-weight: normal;
        font-family: sans-serif;
        line-height: 50px;
        color: darkslategray;
    }

    .alert, .progress {
        display: none;
        margin-bottom: 10px;
    }
</style>
<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Add <?= $this->menuName; ?></h1>
        <div class="pull-right">
            <button class="btn btn-warning btn-flat" type="button"
                    onclick="location.href='<?= base_url($this->cUri) ?>'">
                <i class="fa fa-undo"></i>
                Back <?= $this->menuName; ?>
            </button>
        </div>
    </div>

    <!-- Content Row -->
    <div class="row">
        <div class="col-xl-12 col-md-12 mb-4">
            <form id="program-form" class="form-signin" method="POST">
                <div class="row">
                    <div class="col-sm-12">

                        <div class="form-group row">
                            <label class="col-sm-3" for="kategori_program">Kategori Program</label>
                            <div class="col-sm-3">
                                <select id="kategori_program" name="kategori_program" class="form-control" required>
                                    <option
                                        value="Umroh" <?= ($model) ? ($model->kategori_program == 'Umroh' ? 'selected' : '') : ''; ?>>
                                        Umroh
                                    </option>
                                    <option
                                        value="Safari Tadabbur" <?= ($model) ? ($model->kategori_program == 'Safari Tadabbur' ? 'selected' : '') : ''; ?> >
                                        Safari Tadabbur
                                    </option>
                                </select>
                            </div>
                            <label class="col-sm-2" for="nama_program">Nama Program</label>
                            <div class="col-sm-4">
                                <input type="hidden" id="program_id" name="program_id"
                                       value="<?= ($model) ? $model->program_id : '' ?>">

                                <input type="text" id="nama_program" name="nama_program"
                                       value="<?= ($model) ? $model->nama_program : '' ?>"
                                       class="form-control" placeholder="Nama Program" required>
                            </div>

                        </div>

                        <div class="form-group row">
                            <div class="col-sm-12">
                                <label for="deskripsi">Deskripsi</label>
                                <div class="position-relative icon-form-control">
                                    <progress style="display: none"></progress>
                                    <div id="summernote"><?= ($model) ? $model->deskripsi : '' ?></div>

                                </div>

                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-3 control-label no-padding-right" for="tanggal_keberangkatan">Tanggal
                                Keberangkatan <a
                                    class="label_required"></a></label>
                            <div class="col-sm-3">
                                <input type="text" class="form-control datepicker" name="tanggal_keberangkatan"
                                       id="tanggal_keberangkatan"
                                       value="<?= ($model) ? $model->tanggal_keberangkatan : '' ?>" required>
                            </div>
                            <label class="col-sm-2" for="durasi">Durasi</label>
                            <div class="col-sm-4">
                                <input type="text" id="durasi" name="durasi"
                                       value="<?= ($model) ? $model->durasi : '' ?>"
                                       class="form-control numbersOnly" placeholder="Durasi" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-3" for="kota_keberangkatan">Kota Keberangkatan</label>
                            <div class="col-sm-3">
                                <input type="text" id="kota_keberangkatan" name="kota_keberangkatan"
                                       value="<?= ($model) ? $model->kota_keberangkatan : '' ?>"
                                       class="form-control" placeholder="Kota Keberangkatan" required>
                            </div>
                            <label class="col-sm-2" for="kota_tujuan">Kota Tujuan</label>
                            <div class="col-sm-4">
                                <input type="text" id="kota_tujuan" name="kota_tujuan"
                                       value="<?= ($model) ? $model->kota_tujuan : '' ?>"
                                       class="form-control" placeholder="Kota Tujuan" required>
                            </div>

                        </div>

                        <div class="form-group row">
                            <label class="col-sm-3" for="quota">Quota</label>
                            <div class="col-sm-3">
                                <input type="text" id="quota" name="quota"
                                       value="<?= ($model) ? $model->quota : '' ?>"
                                       class="form-control numbersOnly" placeholder="Quota" required>
                            </div>
                            <label class="col-sm-2" for="nominal">Nominal</label>
                            <div class="col-sm-4">
                                <input type="text" id="nominal" name="nominal"
                                       value="<?= ($model) ? $model->nominal : '' ?>"
                                       class="form-control numbersOnly" placeholder="Nominal" required>
                            </div>
                        </div>


                        <div class="form-group row">
                            <label class="col-sm-3" for="banner">Banner </label>
                            <div class="col-sm-3">
                                <input type="file" name="userfile" id="userfile" style="display: none">
                                <div class="upload-area" id="uploadfile" style="display: table;">
                                    <div style="top:50%;height: 50%;;display: table-cell;vertical-align: middle;">
                                        Click / Drag Image in here ...
                                        <h4 style="color: #b90d09;">Banner</h4>
                                    </div>
                                </div>

                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0"
                                         aria-valuemax="100"
                                         style="width: 0%;"></div>
                                </div>
                                <div class="alert alert-success" role="alert">Upload Berhasil</div>
                            </div>
                            <div class="col-sm-6">
                                <div class="col-md-12">
                                    <input type="hidden" id="banner"
                                           name="banner" value="<?= ($model) ? $model->banner : '' ?>">
                                    <img class="img-responsive" style="width: 80%; height: 200px;" id="img-banner"
                                        <?
                                        if($model) {
                                            ?>
                                            src="<?= base_url() . "data/" . $model->banner ?>"
                                        <?
                                        } ?>

                                    >
                                </div>
                            </div>


                        </div>

                        <div class="form-group row">
                            <div class="col-sm-6">
                                <button class="btn btn-primary " type="submit">
                                    <i class="ace-icon fa fa-check bigger-110"></i>
                                    Submit
                                </button>
                            </div>

                        </div>

                    </div>
                </div>
            </form>
        </div>
    </div>


</div>
<link href="<?= base_url() ?>assets/vendor/summernote/summernote.min.css" rel="stylesheet">
<script src="<?= base_url() ?>assets/vendor/summernote/summernote.min.js"></script>
<link href="<?= base_url() ?>assets/vendor/summernote/emoji.css" rel="stylesheet">
<script src="<?= base_url() ?>assets/vendor/summernote/config.js"></script>
<script src="<?= base_url() ?>assets/vendor/summernote/tam-emoji.min.js"></script>

<style>
    .icon-form-control i {
        display: flex;
        top: 0;
        bottom: 0;
        align-items: center;
        font-size: 14px;
        justify-content: center;
        width: 20px;
    }

    .note-toolbar {
        margin: 0;
        padding: 0 0 5px 5px;
        background: #f8f9fa;
    }

</style>

<!-- /.container-fluid -->

<script>
    var $progress = $('.progress');
    var $progressBar = $('.progress-bar');
    var $alert = $('.alert');
    var validator = $('#data').validate();

    $(document).ready(function () {
        document.emojiButton = 'fa fa-smile-o'; // default: fa fa-smile-o

        document.emojiSource = '<?=base_url()?>assets/vendor/summernote/img/';

        $("#summernote").summernote({
            height: 150,
            toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'italic', 'underline', 'clear']],
                ['fontname', ['fontname']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']],
                ['table', ['table']],
                ['insert', ['link', 'picture', 'hr']],
                ['view', ['fullscreen', 'codeview']],
                ['help', ['help']],
                ['insert', ['emoji']],
                ['tool', ['undo', 'redo']]
            ],
            callbacks: {
                onImageUpload: function (image) {
                    bootbox.dialog({
                        title: '<?=$title?>',
                        message: '<p>Jadikan gambar sebagai Icon post</p>',
                        size: 'small',
                        onEscape: false,
                        backdrop: true,
                        buttons: {
                            fee: {
                                label: 'Tidak',
                                className: 'btn-danger',
                                callback: function () {
                                    sendFile(image[0], false);
                                }
                            },
                            fi: {
                                label: 'Ya',
                                className: 'btn-info',
                                callback: function () {
                                    sendFile(image[0], true);
                                }
                            }
                        }
                    });

                }
            }
        });

        function sendFile(file, isIcon) {
            data = new FormData();
            data.append("file", file);
            $.ajax({
                data: data,
                type: 'POST',
                xhr: function () {
                    var myXhr = $.ajaxSettings.xhr();
                    if (myXhr.upload) myXhr.upload.addEventListener('progress', progressHandlingFunction, false);
                    return myXhr;
                },
                url: '<?=base_url()?>image/upload',
                cache: false,
                contentType: false,
                processData: false,
                success: function (url) {
                    var image = $('<img>').attr('src', url);
                    $('#summernote').summernote("insertNode", image[0]);
                    if (isIcon) {
                        $('#image').val(url);
                    }
                }
            });
        }

        function progressHandlingFunction(e) {
            if (e.lengthComputable) {
                $('progress').show();
                $('progress').attr({value: e.loaded, max: e.total});
                // reset progress on complete
                if (e.loaded == e.total) {
                    $('progress').attr('value', '0.0');
                    $('progress').hide();
                }
            }
        }

        $('#program-form').submit(function (event) {
            event.preventDefault();

            $.blockUI();
            $.ajax({
                url: '<?=$cUri?>/save',
                type: 'POST',
                data: $('#program-form').serialize() + '&deskripsi=' + encodeURIComponent($('#summernote').summernote('code')),
                success: function (data) {
                    alert(data.message)
                    location.href = data.redirect;
                    $.unblockUI();
                },
                error: function (response) {
                    message(response.responseText);
                    $.unblockUI();
                }
            });

        });
    });

    $(function () {
        // preventing page from redirecting
        $("html").on("dragover", function (e) {
            e.preventDefault();
            e.stopPropagation();
            $("h1").text("Drag here");
        });

        $("html").on("drop", function (e) {
            e.preventDefault();
            e.stopPropagation();
        });

        // Drag enter
        $('.upload-area').on('dragenter', function (e) {
            e.stopPropagation();
            e.preventDefault();
            $("h1").text("Drop");
        });

        // Drag over
        $('.upload-area').on('dragover', function (e) {
            e.stopPropagation();
            e.preventDefault();
            $("h1").text("Drop");
        });

        // Drop
        $('.upload-area').on('drop', function (e) {
            e.stopPropagation();
            e.preventDefault();

            $("h1").text("Upload");

            var file = e.originalEvent.dataTransfer.files;
            var fd = new FormData();
            fd.append('userfile', file[0]);
            uploadData(fd);
        });

        // Open file selector on div click
        $("#uploadfile").click(function () {
            $("#userfile").click();
        });

        // file selected
        $("#userfile").change(function () {
            var fd = new FormData();
            var files = $('#userfile')[0].files[0];
            fd.append('userfile', files);
            uploadData(fd);
        });
    });

    function uploadData(formdata) {
        console.log('starting ajax request');
        $progress.css('display', 'block');
        $.ajax({
            xhr: function () {
                var xhr = new window.XMLHttpRequest();
                xhr.upload.addEventListener("progress", function (evt) {
                    if (evt.lengthComputable) {
                        var percentComplete = evt.loaded / evt.total;
                        //Do something with upload progress
                        console.log(percentComplete);
                        $progressBar.css('width', (percentComplete * 100) + '%');

                        if (percentComplete == 1) {
                            $progress.css('display', 'none');
                            $alert.css('display', 'block');
                        }
                    }
                }, false);
                //Download progress
                xhr.addEventListener("progress", function (evt) {
                    if (evt.lengthComputable) {
                        var percentComplete = evt.loaded / evt.total;
                        console.log(percentComplete);
                        $progressBar.css('width', (percentComplete * 100) + '%');

                        if (percentComplete == 1) {
                            $progress.css('display', 'none');
                            $alert.css('display', 'block');
                        }
                    }
                }, false);
                return xhr;
            },
            url: '<?=base_url()?>image/upload',
            type: 'post',
            data: formdata,
            contentType: false,
            processData: false,
            success: function (response) {
                $('#banner').val(response);
                $("#img-banner").attr("src", "<?=base_url()?>data/" + response);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                message(errorThrown);
            }
        });
    }

</script>