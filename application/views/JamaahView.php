<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
<!--        <h1 class="h3 mb-0 text-gray-800">Anggota Keluarga</h1>-->
        <?=$breadcrumb?>

        <div class="pull-right">
            <button class="btn btn-primary btn-flat" type="button" onclick="location.href='<?= base_url() ?>jamaah/form/<?=getSessionUserId()?>/<?=$this->uri->segment(3)?>'">
                <i class="fa fa-user-plus"></i>
                Tambah Anggota Keluarga
            </button>
        </div>
    </div>

    <div class="card shadow mb-4">
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Nama</th>
                        <th>Nomor Paspor</th>
                        <th>HP</th>
                        <th>Email</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>

                    <?php $no = 1;
                    foreach ($dataTable as $jamaah) {
                        ?>
                        <tr>
                            <td><?=$no++?>.</td>
                            <td><?= $jamaah->nama_lengkap ?></td>
                            <td><?= $jamaah->no_paspor ?></td>
                            <td><?= $jamaah->no_hp ?></td>
                            <td><?= $jamaah->email ?></td>
                            <td>
                                <button class="btn btn-warning btn-sm" type="button" onclick="doEdit(<?=$jamaah->user_id ?>, <?=$jamaah->jamaah_id ?>)">
                                    <i class="fa fa-edit"></i>
                                    Edit
                                </button>

                                <button class="btn btn-success btn-sm" type="button" onclick="daftarkanProgram(<?=$this->uri->segment(3)?>, <?=$jamaah->jamaah_id ?>)">
                                    <i class="fa fa-user-check"></i>
                                    Daftarkan Program
                                </button>
                            </td>

                        </tr>
                        <?
                    }
                    ?>


                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>

<!-- /.container-fluid -->

<script>
    $(document).ready(function () {
        $('#dataTable').DataTable();
    });


    function doEdit(userId, jamaahId) {
        location.href = '<?=base_url($this->cUri)?>/form/' + userId + '/' + <?=$this->uri->segment(3)?> + '/' + jamaahId;
    }

    function daftarkanProgram(programUserId, jamaahId) {
        $.blockUI();
        $.ajax({
            type: 'GET',
            url: '<?=base_url()?>jamaah/daftarkanProgram/' + programUserId + '/' + jamaahId,
            success: function (res) {
                $.unblockUI();
                if (res.success) {
                    Swal.fire({
                        title: '',
                        text: res.message,
                        icon: 'success',
                        showCancelButton: false,
                        allowEscapeKey: false,
                        allowOutsideClick: false,
                    }).then((result) => {
                        if (result.value) {
                            window.location = res.redirect;
                    }
                })
                } else {
                    Swal.fire('',res.message,'error')
                }
            }, error: function (res) {
                $.unblockUI();
                Swal.fire('','Connection Error','error')
            }
        });
    }

    function doDelete($id) {
        if (confirm("Are you sure to delete!") == true) {
            $.ajax({
                type: "GET",
                url: '<?=$cUri; ?>/delete/' + $id,
                success: function (result) {
                    if (result.success) {
                        alert(result.message)
                        window.location.reload();
                    }
                }
            });

        }
    }


</script>