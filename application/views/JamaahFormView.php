<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <?=$breadcrumb?>
    </div>

    <!-- Content Row -->
    <div class="row">
        <div class="col-xl-12 col-md-12 mb-4">
            <form id="data" class="form-signin " method="POST">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group row">
                            <label class="col-sm-3" for="nama_lengkap">Nama Sesuai PASPOR<a
                                    class="label_required">*</a></label>
                            <div class="col-sm-9">

                                <input type="hidden" id="jamaah_id" name="jamaah_id" value="<?= ($model) ? $model->jamaah_id : '' ?>">
                                <input type="hidden" id="user_id" name="user_id" value="<?= $this->uri->segment(3)?>">
                                <input type="hidden" id="program_user_id" name="program_user_id" value="<?= $this->uri->segment(4)?>">

                                <input type="text" id="nama_lengkap" name="nama_lengkap"
                                       value="<?= ($model) ? $model->nama_lengkap : '' ?>"
                                       class="form-control" placeholder="Nama Sesuai PASPOR" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-3" for="nama_ayah">Nama Ayah Kandung<a
                                    class="label_required">*</a></label>
                            <div class="col-sm-9">
                                <input type="text" id="nama_ayah" name="nama_ayah"
                                       value="<?= ($model) ? $model->nama_ayah : '' ?>"
                                       class="form-control" placeholder="Nama Ayah Kandung" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-3" for="tempat_lahir">Tempat Lahir<a
                                    class="label_required"></a></label>
                            <div class="col-sm-3">
                                <input type="text" id="tempat_lahir" name="tempat_lahir"
                                       value="<?= ($model) ? $model->tempat_lahir : '' ?>"
                                       class="form-control" placeholder="Tempat Lahir" >
                            </div>
                            <label class="col-sm-2" for="tanggal_lahir">Tanggal Lahir <a
                                    class="label_required"></a></label>
                            <!--<input type="text" class="form-control pull-right" name="tanggal_lahir"
                                       id="tanggal_lahir" value="<? /*= ($model) ? $model->tanggal_lahir : '' */ ?>">-->

                            <div class="col-sm-1">
                                <select class="form-control select2" id="tanggal_lahir"
                                        name="tanggal_lahir" >
                                    <option value="">Tgl</option>
                                    <?

                                    if ($model) {
                                        $tahunLahir = substr($model->tanggal_lahir, 0,4);
                                        $bulanLahir = substr($model->tanggal_lahir, 5, 2);
                                        $tanggalLahir = substr($model->tanggal_lahir, 8, 2);
                                    }

                                    for ($i = 1; $i <= 31; $i++) {
                                        ?>
                                        <option value="<?= substr("0" . $i, -2); ?>"
                                            <?
                                            if($model) {
                                                if($tanggalLahir==$i) {
                                                    echo "selected";
                                                }
                                            }
                                            ?>
                                        ><?= $i; ?></option>
                                        <?
                                    }
                                    ?>
                                </select>
                            </div>

                            <div class="col-sm-1">
                                <select class="form-control select2" id="bulan_lahir"
                                        name="bulan_lahir" >
                                    <option value="">Bln</option>
                                    <?
                                    $bulan = array("Jan", "Feb", "Mar", "Apr", "Mei", "Jun", "Jul", "Agu", "Sep", "Okt", "Nov", "Des");
                                    $bulanNo = array("01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12");
                                    for ($i = 0; $i < count($bulan); $i++) {
                                        ?>
                                        <option value="<?= $bulanNo[$i] ?>"
                                            <?
                                            if($model) {
                                                if($bulanLahir==$bulanNo[$i]) {
                                                    echo "selected";
                                                }
                                            }
                                            ?>
                                        ><?= $bulan[$i] ?></option>
                                        <?
                                    }
                                    ?>
                                </select>
                            </div>

                            <div class="col-sm-2">
                                <select class="form-control select2" id="tahun_lahir"
                                        name="tahun_lahir" >
                                    <option value="">Thn</option>
                                    <?
                                    for ($i = 1960; $i <= 2020; $i++) {
                                        ?>
                                        <option value="<?= $i; ?>"
                                            <?
                                                if($model) {
                                                    if($tahunLahir==$i) {
                                                        echo "selected";
                                                    }
                                                }
                                            ?>

                                        ><?= $i; ?></option>
                                        <?
                                    }
                                    ?>
                                </select>
                            </div>

                        </div>

                        <div class="form-group row">
                            <label class="col-sm-3" for="jenis_kelamin">Jenis Kelamin<a
                                    class="label_required"></a></label>
                            <div class="col-sm-3">
                                <select id="jenis_kelamin" name="jenis_kelamin" class="form-control" >
                                    <option value="">Jenis Kelamin</option>
                                    <option
                                        value="Laki-laki" <?= ($model) ? ($model->jenis_kelamin == 'Laki-laki' ? 'selected' : '') : ''; ?>>
                                        Laki-laki
                                    </option>
                                    <option
                                        value="Perempuan" <?= ($model) ? ($model->jenis_kelamin == 'Perempuan' ? 'selected' : '') : ''; ?> >
                                        Perempuan
                                    </option>
                                </select>
                            </div>

                            <!--<label class="col-sm-1" for="umur">Umur<a class="label_required"></a></label>
                            <div class="col-sm-2">
                                <input type="text" id="umur" name="umur"
                                       value="<? /*= ($model) ? $model->umur : '' */ ?>"
                                       class="form-control numbersOnly" placeholder="Umur" >
                            </div>-->

                            <label class="col-sm-2" for="gol_darah">Golongan darah<a
                                    class="label_required"></a></label>
                            <div class="col-sm-4">
                                <select id="gol_darah" name="gol_darah" class="form-control" >
                                    <option value="">Golongan Darah</option>
                                    <option
                                        value="A" <?= ($model) ? ($model->gol_darah == 'A' ? 'selected' : '') : ''; ?>>A
                                    </option>
                                    <option
                                        value="B" <?= ($model) ? ($model->gol_darah == 'B' ? 'selected' : '') : ''; ?> >
                                        B
                                    </option>
                                    <option
                                        value="AB" <?= ($model) ? ($model->gol_darah == 'AB' ? 'selected' : '') : ''; ?>>
                                        AB
                                    </option>
                                    <option
                                        value="O" <?= ($model) ? ($model->gol_darah == 'O' ? 'selected' : '') : ''; ?> >
                                        O
                                    </option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-3" for="status_perkawinan">Status Perkawinan<a
                                    class="label_required"></a></label>
                            <div class="col-sm-3">
                                <select id="status_perkawinan" name="status_perkawinan" class="form-control" >
                                    <option value="">Status Perkawinan</option>
                                    <option
                                        value="Belum Menikah" <?= ($model) ? ($model->status_perkawinan == 'Belum Menikah' ? 'selected' : '') : ''; ?>>
                                        Belum Menikah
                                    </option>
                                    <option
                                        value="Menikah" <?= ($model) ? ($model->status_perkawinan == 'Menikah' ? 'selected' : '') : ''; ?> >
                                        Menikah
                                    </option>
                                    <option
                                        value="Janda / Duda" <?= ($model) ? ($model->status_perkawinan == 'Janda / Duda' ? 'selected' : '') : ''; ?> >
                                        Janda / Duda
                                    </option>
                                </select>
                            </div>

                            <label class="col-sm-2" for="kewarganegaraan">Kewarganegaraan<a
                                    class="label_required"></a></label>
                            <div class="col-sm-4">
                                <select id="kewarganegaraan" name="kewarganegaraan" class="form-control" >
                                    <option
                                        value="Indonesia" <?= ($model) ? ($model->kewarganegaraan == 'Indonesia' ? 'selected' : '') : ''; ?>>
                                        Indonesia
                                    </option>
                                    <option
                                        value="Asing" <?= ($model) ? ($model->kewarganegaraan == 'Asing' ? 'selected' : '') : ''; ?> >
                                        Asing
                                    </option>

                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-3" for="no_paspor">Nomor Paspor<a
                                    class="label_required"></a></label>
                            <div class="col-sm-3">
                                <input type="text" id="no_paspor" name="no_paspor"
                                       value="<?= ($model) ? $model->no_paspor : '' ?>"
                                       class="form-control " placeholder="Nomor Paspor" >
                            </div>
                            <label class="col-sm-2" for="tanggal_pengeluaran_paspor">Tanggal Pengeluaran <a
                                    class="label_required"></a></label>
                            <!--<div class="col-ms-4">
                                <input type="text" class="form-control pull-right" name="tanggal_pengeluaran_paspor"
                                       id="tanggal_pengeluaran_paspor"
                                       value="<? /*= ($model) ? $model->tanggal_pengeluaran_paspor : '' */ ?>">
                            </div>-->

                            <div class="col-sm-1">
                                <select class="form-control select2" id="tanggal_pengeluaran_paspor"
                                        name="tanggal_pengeluaran_paspor" >
                                    <option value="">Tgl</option>
                                    <?

                                    if ($model) {
                                        $tahunPengeluaran = substr($model->tanggal_pengeluaran_paspor, 0,4);
                                        $bulanPengeluaran = substr($model->tanggal_pengeluaran_paspor, 5, 2);
                                        $tanggalPengeluaran = substr($model->tanggal_pengeluaran_paspor, 8, 2);
                                    }

                                    for ($i = 1; $i <= 31; $i++) {
                                        ?>
                                        <option value="<?= substr("0" . $i, -2); ?>"
                                            <?
                                            if($model) {
                                                if($tanggalPengeluaran==$i) {
                                                    echo "selected";
                                                }
                                            }
                                            ?>
                                        ><?= $i; ?></option>
                                        <?
                                    }
                                    ?>
                                </select>
                            </div>

                            <div class="col-sm-1">
                                <select class="form-control select2" id="bulan_pengeluaran_paspor"
                                        name="bulan_pengeluaran_paspor" >
                                    <option value="">Bln</option>
                                    <?
                                    $bulan = array("Jan", "Feb", "Mar", "Apr", "Mei", "Jun", "Jul", "Agu", "Sep", "Okt", "Nov", "Des");
                                    $bulanNo = array("01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12");
                                    for ($i = 0; $i < count($bulan); $i++) {
                                        ?>
                                        <option value="<?= $bulanNo[$i] ?>"
                                            <?
                                            if($model) {
                                                if($bulanPengeluaran==$bulanNo[$i]) {
                                                    echo "selected";
                                                }
                                            }
                                            ?>
                                        ><?= $bulan[$i] ?></option>
                                        <?
                                    }
                                    ?>
                                </select>
                            </div>

                            <div class="col-sm-2">
                                <select class="form-control select2" id="tahun_pengeluaran_paspor"
                                        name="tahun_pengeluaran_paspor" >
                                    <option value="">Thn</option>
                                    <?
                                    for ($i = 2000; $i <= date('Y'); $i++) {
                                        ?>
                                        <option value="<?= $i; ?>"
                                            <?
                                            if($model) {
                                                if($tahunPengeluaran==$i) {
                                                    echo "selected";
                                                }
                                            }
                                            ?>
                                        ><?= $i; ?></option>
                                        <?
                                    }
                                    ?>
                                </select>
                            </div>

                        </div>

                        <div class="form-group row">
                            <label class="col-sm-3" for="kantor_paspor">Kantor Paspor<a
                                    class="label_required"></a></label>
                            <div class="col-sm-3">
                                <input type="text" id="kantor_paspor" name="kantor_paspor"
                                       value="<?= ($model) ? $model->kantor_paspor : '' ?>"
                                       class="form-control " placeholder="Kantor Paspor" >
                            </div>
                            <label class="col-sm-2" for="tanggal_kadaluarsa_paspor">Tanggal Kadaluarsa <a
                                    class="label_required"></a></label>

                            <div class="col-sm-1">
                                <select class="form-control select2" id="tanggal_kadaluarsa_paspor"
                                        name="tanggal_kadaluarsa_paspor" >
                                    <option value="">Tgl</option>
                                    <?

                                    if ($model) {
                                        $tahunKadaluarsa = substr($model->tanggal_kadaluarsa_paspor, 0,4);
                                        $bulanKadaluarsa = substr($model->tanggal_kadaluarsa_paspor, 5, 2);
                                        $tanggalKadaluarsa = substr($model->tanggal_kadaluarsa_paspor, 8, 2);
                                    }

                                    for ($i = 1; $i <= 31; $i++) {
                                        ?>
                                        <option value="<?= substr("0" . $i, -2); ?>"
                                            <?
                                            if($model) {
                                                if($tanggalKadaluarsa==$i) {
                                                    echo "selected";
                                                }
                                            }
                                            ?>
                                        ><?= $i; ?></option>
                                        <?
                                    }
                                    ?>
                                </select>
                            </div>

                            <div class="col-sm-1">
                                <select class="form-control select2" id="bulan_kadaluarsa_paspor"
                                        name="bulan_kadaluarsa_paspor" >
                                    <option value="">Bln</option>
                                    <?
                                    $bulan = array("Jan", "Feb", "Mar", "Apr", "Mei", "Jun", "Jul", "Agu", "Sep", "Okt", "Nov", "Des");
                                    $bulanNo = array("01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12");
                                    for ($i = 0; $i < count($bulan); $i++) {
                                        ?>
                                        <option value="<?= $bulanNo[$i] ?>"
                                            <?
                                            if($model) {
                                                if($bulanKadaluarsa==$bulanNo[$i]) {
                                                    echo "selected";
                                                }
                                            }
                                            ?>
                                        ><?= $bulan[$i] ?></option>
                                        <?
                                    }
                                    ?>
                                </select>
                            </div>

                            <div class="col-sm-2">
                                <select class="form-control select2" id="tahun_kadaluarsa_paspor"
                                        name="tahun_kadaluarsa_paspor" >
                                    <option value="">Thn</option>
                                    <?
                                    for ($i = 2000; $i <= date('Y'); $i++) {
                                        ?>
                                        <option value="<?= $i; ?>"
                                            <?
                                            if($model) {
                                                if($tahunKadaluarsa==$i) {
                                                    echo "selected";
                                                }
                                            }
                                            ?>
                                        ><?= $i; ?></option>
                                        <?
                                    }
                                    ?>
                                </select>
                            </div>

                        </div>

                        <div class="form-group row">
                            <label class="col-sm-3" for="alamat_ktp">Alamat Sesuai KTP<a
                                    class="label_required"></a></label>
                            <div class="col-sm-9">
                                <input type="text" id="alamat_ktp" name="alamat_ktp"
                                       value="<?= ($model) ? $model->alamat_ktp : '' ?>"
                                       class="form-control" placeholder="Alamat Sesuai KTP" >

                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-3" for="propinsi">Provinsi<a
                                    class="label_required"></a></label>
                            <div class="col-sm-4">
                                <select class="form-control select2" id="provinsi_id"
                                        name="provinsi_id" ></select>
                            </div>

                            <label class="col-sm-1" for="no_hp">Kota</label>
                            <div class="col-sm-4">
                                <select class="form-control select2" id="kota_id"
                                        name="kota_id" ></select>
                            </div>


                        </div>

                        <div class="form-group row">
                            <label class="col-sm-3" for="email">E-Mail</label>
                            <div class="col-sm-4">
                                <input type="text" id="email" name="email"
                                       value="<?= ($model) ? $model->email : '' ?>"
                                       class="form-control" placeholder="E-Mail">
                            </div>

                            <label class="col-sm-1" for="no_hp">HP</label>
                            <div class="col-sm-4">
                                <input type="text" id="no_hp" name="no_hp"
                                       value="<?= ($model) ? $model->no_hp : '' ?>"
                                       class="form-control numbersOnly" placeholder="HP">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-3" for="berat_badan">Berat Badan (kg)<a
                                    class="label_required"></a></label>
                            <div class="col-sm-1">
                                <input type="text" id="berat_badan" name="berat_badan"
                                       value="<?= ($model) ? $model->berat_badan : '' ?>"
                                       class="form-control numbersOnly " placeholder="Berat Badan" >
                            </div>
                            <label class="col-sm-2" for="tinggi_badan">Tinggi Badan(cm)<a
                                    class="label_required"></a></label>
                            <div class="col-sm-2">
                                <input type="text" id="tinggi_badan" name="tinggi_badan"
                                       value="<?= ($model) ? $model->tinggi_badan : '' ?>"
                                       class="form-control numbersOnly" placeholder="Tinggi Badan" >
                            </div>

                            <label class="col-sm-2" for="pendidikan">Pendidikan Terakhir<a
                                    class="label_required"></a></label>
                            <div class="col-sm-2">
                                <select id="gol_darah" name="pendidikan" class="form-control" >
                                    <option value="">Pendidikan</option>
                                    <option
                                        value="SD" <?= ($model) ? ($model->pendidikan == 'SD' ? 'selected' : '') : ''; ?>>
                                        SD
                                    </option>
                                    <option
                                        value="SLTP" <?= ($model) ? ($model->pendidikan == 'SLTP' ? 'selected' : '') : ''; ?> >
                                        SLTP
                                    </option>
                                    <option
                                        value="SMA" <?= ($model) ? ($model->pendidikan == 'SMA' ? 'selected' : '') : ''; ?>>
                                        SMA
                                    </option>
                                    <option
                                        value="SM D1/D2/D3" <?= ($model) ? ($model->pendidikan == 'SM D1/D2/D3' ? 'selected' : '') : ''; ?> >
                                        SM D1/D2/D3
                                    </option>
                                    <option
                                        value="S1" <?= ($model) ? ($model->pendidikan == 'S1' ? 'selected' : '') : ''; ?> >
                                        S1
                                    </option>
                                    <option
                                        value="S2" <?= ($model) ? ($model->pendidikan == 'S2' ? 'selected' : '') : ''; ?>>
                                        S2
                                    </option>
                                    <option
                                        value="S3" <?= ($model) ? ($model->pendidikan == 'S3' ? 'selected' : '') : ''; ?> >
                                        S3
                                    </option>
                                </select>
                            </div>

                        </div>

                        <div class="form-group row">
                            <label class="col-sm-3" for="riwayat_penyakit">Riwayat Penyakit</label>
                            <div class="col-sm-9">
                                <input type="text" id="riwayat_penyakit" name="riwayat_penyakit"
                                       value="<?= ($model) ? $model->riwayat_penyakit : '' ?>"
                                       class="form-control" placeholder="Riwayat Penyakit">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-3" for="penyakit_sedang_diderita">Penyakit yang sedang diderita</label>
                            <div class="col-sm-9">
                                <input type="text" id="penyakit_sedang_diderita" name="penyakit_sedang_diderita"
                                       value="<?= ($model) ? $model->penyakit_sedang_diderita : '' ?>"
                                       class="form-control" placeholder="Penyakit yang sedang diderita">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-3" for="pekerjaan">Pekerjaan<a
                                    class="label_required"></a></label>
                            <div class="col-sm-3">
                                <select id="pekerjaan" name="pekerjaan" class="form-control" >
                                    <option value="">Pekerjaan</option>
                                    <option
                                        value="PNS" <?= ($model) ? ($model->pekerjaan == 'PNS' ? 'selected' : '') : ''; ?>>
                                        PNS
                                    </option>
                                    <option
                                        value="TNI" <?= ($model) ? ($model->pekerjaan == 'TNI' ? 'selected' : '') : ''; ?> >
                                        TNI
                                    </option>
                                    <option
                                        value="Petani" <?= ($model) ? ($model->pekerjaan == 'Petani' ? 'selected' : '') : ''; ?> >
                                        Petani
                                    </option>
                                    <option
                                        value="Karyawan BUMN" <?= ($model) ? ($model->pekerjaan == 'Karyawan BUMN' ? 'selected' : '') : ''; ?> >
                                        Karyawan BUMN
                                    </option>
                                    <option
                                        value="Karyawan Swasta" <?= ($model) ? ($model->pekerjaan == 'Karyawan Swasta' ? 'selected' : '') : ''; ?> >
                                        Karyawan Swasta
                                    </option>
                                    <option
                                        value="IRT" <?= ($model) ? ($model->pekerjaan == 'IRT' ? 'selected' : '') : ''; ?> >
                                        IRT
                                    </option>
                                    <option
                                        value="Pelajar/Mahasiswa" <?= ($model) ? ($model->pekerjaan == 'Pelajar/Mahasiswa' ? 'selected' : '') : ''; ?> >
                                        Pelajar/Mahasiswa
                                    </option>
                                    <option
                                        value="Wiraswasta" <?= ($model) ? ($model->pekerjaan == 'Wiraswasta' ? 'selected' : '') : ''; ?> >
                                        Wiraswasta
                                    </option>

                                </select>
                            </div>
                            <label class="col-sm-2" for="nama_perusahaan">Nama Perusahaan</label>
                            <div class="col-sm-4">
                                <input type="text" id="nama_perusahaan" name="nama_perusahaan"
                                       value="<?= ($model) ? $model->nama_perusahaan : '' ?>"
                                       class="form-control " placeholder="Nama Perusahaan">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-3" for="alamat_perusahaan">Alamat Perusahaan</label>
                            <div class="col-sm-9">
                                <input type="text" id="alamat_perusahaan" name="alamat_perusahaan"
                                       value="<?= ($model) ? $model->alamat_perusahaan : '' ?>"
                                       class="form-control" placeholder="Alamat Perusahaan">
                            </div>
                        </div>

                        <hr>

                        <div class="form-group row">
                            <label class="col-sm-3" for="nama_keluarga">Kontak Darurat</label>
                            <div class="col-sm-3"></div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-3" for="nama_keluarga">Nama Keluarga<a
                                    class="label_required"></a></label>
                            <div class="col-sm-3">
                                <input type="text" id="nama_keluarga" name="nama_keluarga"
                                       value="<?= ($model) ? $model->nama_keluarga : '' ?>"
                                       class="form-control" placeholder="Nama Keluarga" >
                            </div>

                            <label class="col-sm-3" for="telp_keluarga">Telp Keluarga<a
                                    class="label_required"></a></label>
                            <div class="col-sm-3">
                                <input type="text" id="telp_keluarga" name="telp_keluarga"
                                       value="<?= ($model) ? $model->telp_keluarga : '' ?>"
                                       class="form-control numbersOnly" placeholder="Telp Keluarga" >
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-3" for="alamat_keluarga">Alamat Keluarga<a
                                    class="label_required"></a></label>
                            <div class="col-sm-9">
                                <input type="text" id="alamat_keluarga" name="alamat_keluarga"
                                       value="<?= ($model) ? $model->alamat_keluarga : '' ?>"
                                       class="form-control" placeholder="Alamat Keluarga" >
                            </div>
                        </div>


                        <div class="form-group row">
                            <label class="col-sm-3" for="alamat_keluarga"></label>
                            <div class="col-sm-3">
                                <button type="submit" class="btn btn-block btn-primary">Submit</button>
                            </div>

                        </div>

                    </div>
                </div>
            </form>
        </div>
    </div>


</div>


<script>
    jQuery('.numbersOnly').keyup(function () {
        this.value = this.value.replace(/[^0-9\.]/g, '');
    });

    $(document).ready(function () {


        $.ajax({
            url: "<?=base_url('provinsi/getAllSelect2')?>",
            dataType: 'json', type: 'GET', cache: false
        }).then(function (result) {
            $('#provinsi_id').select2({
                data: result.data
            }).on('change', function() {
                var data = $("#provinsi_id option:selected").text();
                if(data!='') {
                    getKota();
                }
            });

            <?
            if($model) {
                ?>
            $("#provinsi_id").val(<?= ($model) ? $model->provinsi_id : '' ?>);
            getKota();
            <?
            }else {
                ?>
            $("#provinsi_id").val("")
            <?
            }
            ?>

            $("#provinsi_id").trigger("change");
        });

        $('#data').submit(function (event) {
            event.preventDefault();

            $.blockUI();
            $.ajax({
                type: 'POST',
                url: '<?=$cUri?>/save',
                data: $('#data').serialize(),
                success: function (res) {
                    $.unblockUI();
                    if (res.success) {
                        Swal.fire({
                            title: '',
                            text: res.message,
                            icon: 'success',
                            showCancelButton: false,
                            allowEscapeKey: false,
                            allowOutsideClick: false,
                        }).then((result) => {
                            if (result.value) {
                                window.location = res.redirect;
                        }
                    })
                    } else {
                        Swal.fire(
                            '',
                            res.message,
                            'error'
                        )
                    }
                }, error: function (res) {
                    $.unblockUI();
                    Swal.fire(
                        '',
                        'Connection Error',
                        'error'
                    )
                }
            });
        });
    });

    //Date picker
    //    $('#tanggal_lahir').datepicker({
    //        autoclose: true,
    //        todayHighlight: true,
    //        format: 'yyyy-mm-dd',
    //        orientation: "buttom right",
    //        language: 'id'
    //
    //    })
    //    $('#tanggal_pengeluaran_paspor').datepicker({
    //        autoclose: true,
    //        todayHighlight: true,
    //        format: 'yyyy-mm-dd',
    //        orientation: "buttom right",
    //        language: 'id'
    //
    //    })
    //    $('#tanggal_kadaluarsa_paspor').datepicker({
    //        autoclose: true,
    //        todayHighlight: true,
    //        format: 'yyyy-mm-dd',
    //        orientation: "buttom right",
    //        language: 'id'
    //
    //    })

    function getKota() {
        $.ajax({
            url: "<?=base_url()?>kota/getByProvinsiSelect2/" + $('#provinsi_id').val(),
            dataType: 'json', type: 'GET', cache: false
        }).then(function (result) {
            $('#kota_id').empty().select2({
                data: result.data
            });

            <?
            if($model) {
            ?>
                $("#kota_id").val(<?= ($model) ? $model->kota_id : '' ?>);
            <?
            }else {
            ?>
                $("#kota_id").val("")
            <?
            }
            ?>

            $("#kota_id").trigger("change");
        });
    }


</script>