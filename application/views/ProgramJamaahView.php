<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <?=$breadcrumb?>
<!--        <h1 class="h3 mb-0 text-gray-800">--><?//=$program->nama_program?><!-- # Program Jamaah</h1>-->


        <div class="pull-right">
            <button class="btn btn-primary btn-flat" type="button" onclick="location.href='<?= base_url() ?>jamaah/anggotaKeluarga/<?=$this->uri->segment(3)?>'">
                <i class="fa fa-user-edit"></i>
                Pilih Keluarga
            </button>
        </div>
    </div>

    <div class="card shadow mb-4">
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Nama</th>
                    </tr>
                    </thead>
                    <tbody>

                    <?php $no = 1;
                    foreach ($dataTable as $jamaah) {
                        ?>
                        <tr>
                            <td><?=$no++?></td>
                            <td><?= $jamaah->nama_lengkap ?></td>
                        </tr>
                        <?
                    }
                    ?>


                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>

<!-- /.container-fluid -->

<script>
    $(document).ready(function () {
        $('#dataTable').DataTable();
    });


    function doEdit($id) {
        location.href = '<?=base_url($this->cUri)?>/form/' + $id;
    }

    function doDelete($id) {
        if (confirm("Are you sure to delete!") == true) {
            $.ajax({
                type: "GET",
                url: '<?=$cUri; ?>/delete/' + $id,
                success: function (result) {
                    if (result.success) {
                        alert(result.message)
                        window.location.reload();
                    }
                }
            });

        }
    }


</script>