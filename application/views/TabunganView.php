<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Tabungan</h1>
    </div>

    <div class="row">
        <div class="col-xl-3 col-md-6 mb-4">
            <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">No Tabungan</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $tabungan->no_tabungan ?></div>
                        </div>

                    </div>

                    <hr>

                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Saldo</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800">
                                Rp.<?= thousandSeparator($saldo) ?></div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-money-check fa-2x text-gray-300"></i>
                        </div>
                    </div>

                    <!--<hr>
                    <div class="row no-gutters">
                        <div class="col-12">
                            <a href="<? /*= base_url('tabungan/setor') */ ?>" class="btn btn-primary btn-icon-split btn-block">
                                <span class="text">Top Up</span>
                            </a>
                        </div>
                    </div>-->

                </div>
            </div>
        </div>


        <div class="col-xl-9 col-md-6 mb-4">
            <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">

                    <div class="row no-gutters align-items-center">
                        <div class="col">
                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Target Tabungan</div>
                            <div class="h6 mb-0 font-weight-bold text-gray-800">
                                Rp.<?= thousandSeparator($tabungan->target_tabungan) ?></div>
                        </div>

                        <div class="col">
                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Jangka Waktu</div>
                            <div class="h6 mb-0 font-weight-bold text-gray-800"><?= $tabungan->jangka_waktu ?> Bln</div>
                        </div>
                    </div>

                    <hr>

                    <div class="row no-gutters align-items-center">
                        <div class="col">
                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Jumlah Keluarga</div>
                            <div class="h6 mb-0 font-weight-bold text-gray-800"><?= $tabungan->jumlah_keluarga ?></div>
                        </div>

                        <div class="col">
                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Setoran Bulanan</div>
                            <div class="h6 mb-0 font-weight-bold text-gray-800">
                                Rp.<?= thousandSeparator($tabungan->setoran_bulanan) ?></div>
                        </div>
                    </div>

                    <hr>


                    <div class="row no-gutters align-items-center">
                        <div class="col">
                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Target Total</div>
                            <div class="h6 mb-0 font-weight-bold text-gray-800">
                                Rp.<?= thousandSeparator($tabungan->target_tabungan * $tabungan->jumlah_keluarga) ?></div>
                        </div>

                            <div class="col">
                                <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">VA BNIS</div>
                                <div class="h5 mb-0 font-weight-bold text-gray-800" id="va_bnis"><?= $tabungan->va_bnis ?> <i id="copyButton" class="fas fa-copy text-primary"></i>
                                </div>
                            </div>

                    </div>
                </div>
            </div>
        </div>

    </div>

    <div class="row">
        <div class="offset-xl-3 col-xl-9 col-md-6 mb-4">
            <div class="card border-left-warning shadow h-100 py-2">
                <div class="card-body">

                    <?php
                    foreach ($setorTabunganList as $setorTabungan) {
                        ?>
                        <div class="row no-gutters align-items-center">
                            <div class="col">
                                <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Setoran Ke</div>
                                <div
                                    class="h6 mb-0 font-weight-bold text-gray-800"><?= $setorTabungan->setoran_ke ?></div>
                            </div>

                            <div class="col">
                                <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Nominal</div>
                                <div class="h6 mb-0 font-weight-bold text-gray-800">
                                    Rp.<?= thousandSeparator($setorTabungan->nominal) ?></div>
                            </div>

                            <div class="col">
                                <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Status</div>
                                <div class="h6 mb-0 font-weight-bold text-gray-800"><?= $setorTabungan->status ?></div>
                            </div>

                            <?
                            if ($setorTabungan->status == "SUDAH BAYAR") {
                            ?>
                            <div class="col">
                                <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Tanggal Bayar</div>
                                <div class="h6 mb-0 font-weight-bold text-gray-800"><?= readableDate($setorTabungan->updated_date) ?></div>
                            </div>
                                <?
                            } ?>

                        </div>
                        <hr>
                        <?php
                    }
                    ?>

                </div>
            </div>
        </div>

    </div>


</div>
<!-- /.container-fluid -->


<script>
    document.getElementById("copyButton").addEventListener("click", function () {
        copyToClipboard(document.getElementById("va_bnis"));
    });

</script>