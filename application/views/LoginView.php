<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>AQL PINTAR</title>

    <link href="<?= base_url() ?>assets/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <link href="<?= base_url() ?>assets/css/sb-admin-2.min.css" rel="stylesheet">
    <link href="<?= base_url() ?>assets/vendor/sweetalert2/sweetalert2.min.css" rel="stylesheet">

    <style>
        .bg-gradient-primary {
            background-color: #20ABA6;
            background-image: linear-gradient(180deg, #0f6875 10%, #20ABA6 100%);
            background-size: cover;
        }
    </style>
</head>

<body class="bg-gradient-primary">

    <div class="container">

        <!-- Outer Row -->
        <div class="row justify-content-center">

            <div class="col-xl-4 col-lg-4 col-md-4">

                <div class="card o-hidden border-0 shadow-lg my-5">
                    <div class="card-body p-0">
                        <!-- Nested Row within Card Body -->
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="p-5">
                                    <div class="text-center">
                                        <img class="img-fluid" src="<?= base_url('assets/img/pintar_logo.png') ?>">
                                        <br>
                                        <br>
                                    </div>
                                    <form class="user" id="login-form">
                                        <div class="form-group">
                                            <input type="text" class="form-control form-control-user" id="email" name="email" placeholder="No HP / Email" required>
                                        </div>
                                        <div class="form-group">
                                            <input type="password" class="form-control form-control-user" id="password" name="password" placeholder="Password" required>
                                        </div>
                                        <button type="submit" class="btn btn-primary btn-user btn-block">
                                            Login
                                        </button>
                                        <hr>
                                    </form>
                                    <hr>
                                    <div class="text-center">
                                        <a class="small" href="#">Lupa Password ?</a>
                                    </div>
                                    <div class="text-center">
                                        <a class="small" href="<?= base_url('register') ?>">Buat Akun</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </div>
    <script src="<?= base_url() ?>assets/vendor/jquery/jquery.min.js"></script>
    <script src="<?= base_url() ?>assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="<?= base_url() ?>assets/vendor/jquery-easing/jquery.easing.min.js"></script>
    <script src="<?= base_url() ?>assets/vendor/jquery/jquery.blockUI.js"></script>
    <script src="<?= base_url() ?>assets/vendor/sweetalert2/sweetalert2.min.js"></script>

    <script>
        $('#login-form').submit(function(event) {
            event.preventDefault();

            $.blockUI();
            $.ajax({
                type: 'POST',
                url: '<?= base_url() ?>login/doLogin',
                data: $('#login-form').serialize(),
                success: function(res) {
                    $.unblockUI();
                    if (res.success) {
                        window.location = res.redirect;
                    } else {
                        Swal.fire(
                            '',
                            res.message,
                            'error'
                        )
                    }
                },
                error: function(res) {
                    $.unblockUI();
                    Swal.fire(
                        '',
                        'Connection Error',
                        'error'
                    )
                }
            });
        });
    </script>

</body>

</html>