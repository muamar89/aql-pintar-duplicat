<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800"><?= $this->menuName; ?></h1>
        <div class="pull-right">
            <button class="btn btn-primary btn-flat" type="button" onclick="location.href='<?= base_url($this->cUri) ?>/form'">
                <i class="fa fa-list"></i>
                Add
            </button>
        </div>
    </div>

    <div class="card shadow mb-4">
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Kategori Program</th>
                        <th>Nama Program</th>
                        <th>Tanggal</th>
                        <th>Nominal</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>

                    <?php $no = 1;
                    foreach ($dataTable as $program) {
                        ?>
                        <tr>
                            <td><?=$no++?>.</td>
                            <td><?= $program->kategori_program ?></td>
                            <td><?= $program->nama_program ?></td>
                            <td><?= $program->tanggal_keberangkatan ?></td>
                            <td><?= $program->nominal ?></td>
                            <td>
                                <button onclick="doEdit(<?=$program->program_id ?>)"
                                        type="button" class="btn btn-warning btn-sm"><i class="fa fa-edit"></i></button>
                            </td>

                        </tr>
                        <?
                    }
                    ?>


                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>

<!-- /.container-fluid -->

<script>
    $(document).ready(function () {
        $('#dataTable').DataTable();
    });


    function doEdit($id) {
        location.href = '<?=base_url($this->cUri)?>/form/' + $id;
    }

    function doDelete($id) {
        if (confirm("Are you sure to delete!") == true) {
            $.ajax({
                type: "GET",
                url: '<?=$cUri; ?>/delete/' + $id,
                success: function (result) {
                    if (result.success) {
                        alert(result.message)
                        window.location.reload();
                    }
                }
            });

        }
    }


</script>