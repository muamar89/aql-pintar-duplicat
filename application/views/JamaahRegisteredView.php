<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Jamaah</h1>
    </div>

    <div class="card shadow mb-4">
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Nama</th>
                        <th>Nomor Paspor</th>
                        <th>HP</th>
                        <th>Email</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>

                    <?php $no = 1;
                    foreach ($dataTable as $jamaah) {
                        ?>
                        <tr>
                            <td><?=$no++?>.</td>
                            <td><?= $jamaah->nama_lengkap ?></td>
                            <td><?= $jamaah->no_paspor ?></td>
                            <td><?= $jamaah->no_hp ?></td>
                            <td><?= $jamaah->email ?></td>
                            <td>
                                <button class="btn btn-warning btn-sm" type="button" onclick="doEdit(<?=$jamaah->jamaah_id ?>)">
                                    <i class="fa fa-edit"></i>
                                    Edit
                                </button>
                            </td>

                        </tr>
                        <?
                    }
                    ?>


                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>

<!-- /.container-fluid -->

<script>
    $(document).ready(function () {
        $('#dataTable').DataTable();
    });

    function doEdit(jamaahId) {
        location.href = '<?=base_url($this->cUri)?>/formregistered/' + jamaahId;
    }
</script>